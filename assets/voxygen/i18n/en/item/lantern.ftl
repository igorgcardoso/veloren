
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

lantern-magic_lantern = Magic Lantern
    .desc = 
        Illuminates even the darkest dungeon
        A great monster was slain for this item

lantern-black-0 = Black Lantern
    .desc = Quite common due to popular use of budding adventurers!

lantern-blue-0 = Cool Blue Lantern
    .desc = This lantern is surprisingly cold when lit.

lantern-geode_purp = Purple Geode
    .desc = Emits a calming glow, helps to calm your nerves.

lantern-green-0 = Lime Zest Lantern
    .desc = It has an opening that could fit a ring...

lantern-polaris = Polaris
    .desc = Christmas Lantern.

lantern-pumpkin = Eerie Pumpkin
    .desc = Did it just blink?!

lantern-red-0 = Red Lantern
    .desc = Caution: contents hot
