
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

weapon-shield-wood-0 = A Tattered Targe
    .desc = Should withstand a few more hits, hopefully...

weapon-dagger-dagger_basic-0 = Suspicious Paper Knife
    .desc = Opens letters quickly.

weapon-dagger-dagger_cult-0 = Magical Cultist Dagger
    .desc = This belonged to an evil Cult Leader.

weapon-dagger-dagger_rusty = Rusty Dagger
    .desc = Easily concealed.

weapon-bow-sagitta = Sagitta
    .desc = Said to have slain a dragon with a single arrow

weapon-bow-starter = Uneven Bow
    .desc = Someone carved their initials into it.

weapon-bow-velorite = Velorite Bow
    .desc = Infused with Velorite power.

weapon-sword-starter_1h = Damaged Gladius
    .desc = This blade has seen better days, but surely it will last.

weapon-axe-2haxe_malachite-0 = Malachite Axe
    .desc = Etched axe head decorated with malachite on the blades to provide magical properties.

weapon-axe-parashu = Parashu
    .desc = Said to be able to cleave the heavens.

weapon-axe-2haxe_rusty = Notched Axe
    .desc = Every dent tells the story of a chopped tree.

weapon-staff-firestaff_cultist = Cultist Staff
    .desc = The fire gives off no heat.

weapon-staff-laevateinn = Laevateinn
    .desc = Can shatter the gate of death

weapon-staff-firestaff_starter = Humble Stick
    .desc = Walking stick with a sharpened end.

weapon-staff-firestaff_starter-starter_staff = Gnarled Rod
    .desc = Smells like resin and magic.

weapon-sword-caladbolg = Caladbolg
    .desc = You sense an eldritch presence watching you.

weapon-sword-cultist = Magical Cultist Greatsword
    .desc = This belonged to an evil Cult Leader.

weapon-sword-frost-0 = Frost Cleaver
    .desc = Radiates a freezing aura.

weapon-sword-frost-1 = Frost Saw
    .desc = Forged from a single piece of eternal ice.

weapon-sword-starter = Damaged Greatsword
    .desc = The blade could snap at any moment, but you hope it will endure future fights.

weapon-tool-broom-0 = Broom
    .desc = It's beginning to fall apart.

weapon-tool-fishing_rod_blue-0 = Fishing Rod
    .desc = Smells of fish.

weapon-tool-golf_club = Golf Club
    .desc = Peasant swatter. Fiercely anti-urbanist. Climate crisis? What climate crisis?

weapon-tool-hoe_green = Hoe
    .desc = It's stained with dirt.

weapon-tool-pickaxe_green-0 = Pickaxe
    .desc = It has a chipped edge.

weapon-tool-pitchfork-0 = Pitchfork
    .desc = One of the prongs is broken.

weapon-tool-rake-0 = Rake
    .desc = Held together with twine.

weapon-tool-shovel_green = Shovel
    .desc = It's covered in manure.

weapon-tool-shovel_gold = Shovel
    .desc = It's been recently cleaned.

weapon-sceptre-amethyst = Amethyst Staff
    .desc = Its stone is the closest thing from perfection

weapon-sceptre-caduceus = Caduceus
    .desc = The snakes seem to be alive

weapon-sceptre-root_evil = The Root of Evil
    .desc = 'Everything comes at a price...'

weapon-sceptre-ore-nature = Velorite Sceptre
    .desc = Heals your allies with the mystical Velorite aura.

weapon-sceptre-wood-simple = Naturalist Walking Stick
    .desc = Heals your allies with the power of nature.

weapon-hammer-burnt_drumstick = Burnt Drumstick
    .desc = Might need more practice...

weapon-hammer-cult_purp-0 = Magical Cultist Warhammer
    .desc = This belonged to an evil Cult Leader.

weapon-hammer-2hhammer_flimsy = Flimsy Hammer
    .desc = The head is barely secured.

weapon-hammer-2hhammer_rusty = Crude Mallet
    .desc = Breaks bones like sticks and stones.

weapon-hammer-2hhammer_mjolnir = Mjolnir
    .desc = It's crackling with lightning.

weapon-hammer-2hhammer_rusty-starter_hammer = Sturdy Old Hammer
    .desc = 'Property of...' The rest is missing.

weapon-tool-broom_belzeshrub_purple = Belzeshrub the Broom-God
    .desc = 
        You can hear him giggle whenever
        you hit the ground a bit too hard...

weapon-sword-frost-1-admin_sword = Admin Greatsword
    .desc = Shouldn't this be a hammer?

weapon-bow-velorite-velorite_bow_debug = Admin Velorite Bow
    .desc = Infused with Velorite power.

weapon-projectile-fireworks_blue-0 = Firework Blue
    .desc = Recommended clearance: 42 chonks

weapon-projectile-fireworks_green-0 = Firework Green
    .desc = Watch out for trees.

weapon-projectile-fireworks_purple-0 = Firework Purple
    .desc = Cult favourite.

weapon-projectile-fireworks_red-0 = Firework Red
    .desc = 
        Humans sometimes use these
        as a flare in a pinch.

weapon-projectile-fireworks_white-0 = Firework White
    .desc = Twinkles like the stars

weapon-projectile-fireworks_yellow-0 = Firework Yellow
    .desc = 
        The Great Doctor passed away after
        testing this contraption indoors.

weapon-hammer-craftsman = Craftsman Hammer
    .desc = Used to craft various items.

weapon-tool-pickaxe_green-1 = Steel Pickaxe
    .desc = Allows for swift excavation of any ore in sight.

weapon-tool-pickaxe_stone = Stone Pickaxe
    .desc = Strike the earth!

weapon-tool-wooden_bass = Double Bass
    .desc = Wooden Bass.

weapon-tool-wooden_flute = Flute
    .desc = Wooden Flute.

weapon-tool-glass_flute = Glass Flute
    .desc = What's the Cardinal doing with it?

weapon-tool-wooden_guitar = Guitar
    .desc = Wooden Guitar.

weapon-tool-black_velvet_guitar = Dark Guitar
    .desc = Sounds edgy.

weapon-tool-icy_talharpa = Icy Talharpa
    .desc = Icy Talharpa.

weapon-tool-wooden_kalimba = Kalimba
    .desc = Wooden Kalimba.

weapon-tool-wooden_lute = Lute
    .desc = Wooden Lute.

weapon-tool-wooden_lyre = Lyre
    .desc = Wooden Lyre.

weapon-tool-melodica = Melodica
    .desc = Wooden Melodica.

weapon-tool-wooden_sitar = Sitar
    .desc = Wooden Sitar.

weapon-tool-steeldrum = Steeldrum
    .desc = Steeldrum.

weapon-tool-shamisen = Shamisen
    .desc = Shamisen.

weapon-tool-washboard = Washboard
    .desc = Washboard.

weapon-tool-wildskin_drum = Wildskin Drum
    .desc = one, two, you know what to do!

weapon-component-bow-grip-long = Long Bow Grip
    .desc = {""}

weapon-component-bow-grip-medium = Medium Bow Grip
    .desc = {""}

weapon-component-bow-grip-short = Short Bow Grip
    .desc = {""}

weapon-component-axe-haft-long = Long Axe Haft
    .desc = {""}

weapon-component-axe-haft-medium = Medium Axe Haft
    .desc = {""}

weapon-component-axe-haft-short = Short Axe Haft
    .desc = {""}

weapon-component-staff-core-heavy = Heavy Pyrocore
    .desc = {""}

weapon-component-staff-core-light = Light Pyrocore
    .desc = {""}

weapon-component-staff-core-medium = Pyrocore
    .desc = {""}

weapon-component-sword-hilt-long = Long Sword Hilt
    .desc = {""}

weapon-component-sword-hilt-medium = Medium Sword Hilt
    .desc = {""}

weapon-component-sword-hilt-short = Short Sword Hilt
    .desc = {""}

weapon-component-sceptre-core-heavy = Heavy Biocore
    .desc = {""}

weapon-component-sceptre-core-light = Light Biocore
    .desc = {""}

weapon-component-sceptre-core-medium = Biocore
    .desc = {""}

weapon-component-hammer-shaft-long = Long Hammer Haft
    .desc = {""}

weapon-component-hammer-shaft-medium = Medium Hammer Haft
    .desc = {""}

weapon-component-hammer-shaft-short = Short Hammer Haft
    .desc = {""}

weapon-component-sword-greatsword-iron = Iron Greatsword Blade
    .desc = {""}

weapon-component-sword-katana-iron = Iron Katana Blade
    .desc = {""}

weapon-component-sword-sawblade-iron = Iron Sawblade
    .desc = {""}

weapon-component-sword-sabre-iron = Iron Sabre Blade
    .desc = {""}

weapon-component-sword-ornate-iron = Iron Ornate Sword Blade
    .desc = {""}

weapon-component-sword-longsword-iron = Iron Longsword Blade
    .desc = {""}

weapon-component-sword-zweihander-iron = Iron Zweihander Blade
    .desc = {""}

weapon-component-bow-shortbow-wood = Wooden Shortbow Limbs
    .desc = {""}

weapon-component-bow-longbow-wood = Wooden Longbow Limbs
    .desc = {""}

weapon-component-bow-ornate-wood = Wooden Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-greatbow-wood = Wooden Greatbow Limbs
    .desc = {""}

weapon-component-bow-composite-wood = Wooden Composite Bow Limbs
    .desc = {""}

weapon-component-bow-bow-wood = Wooden Bow Limbs
    .desc = {""}

weapon-component-bow-warbow-wood = Wooden Warbow Limbs
    .desc = {""}

weapon-component-hammer-maul-bronze = Bronze Maul Head
    .desc = {""}

weapon-component-hammer-hammer-bronze = Bronze Hammer Head
    .desc = {""}

weapon-component-hammer-greatmace-bronze = Bronze Greatmace Head
    .desc = {""}

weapon-component-hammer-spikedmace-bronze = Bronze Spiked Mace Head
    .desc = {""}

weapon-component-hammer-greathammer-bronze = Bronze Greathammer Head
    .desc = {""}

weapon-component-hammer-warhammer-bronze = Bronze Warhammer Head
    .desc = {""}

weapon-component-hammer-ornate-bronze = Bronze Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-maul-bloodsteel = Bloodsteel Maul Head
    .desc = {""}

weapon-component-hammer-greathammer-bloodsteel = Bloodsteel Greathammer Head
    .desc = {""}

weapon-component-hammer-ornate-bloodsteel = Bloodsteel Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-hammer-bloodsteel = Bloodsteel Hammer Head
    .desc = {""}

weapon-component-hammer-spikedmace-bloodsteel = Bloodsteel Spiked Mace Head
    .desc = {""}

weapon-component-hammer-greatmace-bloodsteel = Bloodsteel Greatmace Head
    .desc = {""}

weapon-component-hammer-warhammer-bloodsteel = Bloodsteel Warhammer Head
    .desc = {""}

weapon-component-sceptre-ornate-frostwood = Frostwood Ornate Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-sceptre-frostwood = Frostwood Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-crook-frostwood = Frostwood Crook Shaft
    .desc = {""}

weapon-component-sceptre-grandsceptre-frostwood = Frostwood Grandsceptre Shaft
    .desc = {""}

weapon-component-sceptre-cane-frostwood = Frostwood Cane Shaft
    .desc = {""}

weapon-component-sceptre-crozier-frostwood = Frostwood Crozier Shaft
    .desc = {""}

weapon-component-sceptre-arbor-frostwood = Frostwood Arbor Shaft
    .desc = {""}

weapon-component-bow-bow-eldwood = Eldwood Bow Limbs
    .desc = {""}

weapon-component-bow-warbow-eldwood = Eldwood Warbow Limbs
    .desc = {""}

weapon-component-bow-greatbow-eldwood = Eldwood Greatbow Limbs
    .desc = {""}

weapon-component-bow-ornate-eldwood = Eldwood Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-longbow-eldwood = Eldwood Longbow Limbs
    .desc = {""}

weapon-component-bow-composite-eldwood = Eldwood Composite Bow Limbs
    .desc = {""}

weapon-component-bow-shortbow-eldwood = Eldwood Shortbow Limbs
    .desc = {""}

weapon-component-axe-axe-bloodsteel = Bloodsteel Axe Head
    .desc = {""}

weapon-component-axe-ornate-bloodsteel = Bloodsteel Ornate Axe Head
    .desc = {""}

weapon-component-axe-jagged-bloodsteel = Bloodsteel Jagged Axe Head
    .desc = {""}

weapon-component-axe-battleaxe-bloodsteel = Bloodsteel Battleaxe Head
    .desc = {""}

weapon-component-axe-greataxe-bloodsteel = Bloodsteel Greataxe Head
    .desc = {""}

weapon-component-axe-labrys-bloodsteel = Bloodsteel Labrys Head
    .desc = {""}

weapon-component-axe-poleaxe-bloodsteel = Bloodsteel Poleaxe Head
    .desc = {""}

weapon-component-axe-poleaxe-cobalt = Cobalt Poleaxe Head
    .desc = {""}

weapon-component-axe-jagged-cobalt = Cobalt Jagged Axe Head
    .desc = {""}

weapon-component-axe-labrys-cobalt = Cobalt Labrys Head
    .desc = {""}

weapon-component-axe-axe-cobalt = Cobalt Axe Head
    .desc = {""}

weapon-component-axe-ornate-cobalt = Cobalt Ornate Axe Head
    .desc = {""}

weapon-component-axe-greataxe-cobalt = Cobalt Greataxe Head
    .desc = {""}

weapon-component-axe-battleaxe-cobalt = Cobalt Battleaxe Head
    .desc = {""}

weapon-component-hammer-maul-iron = Iron Maul Head
    .desc = {""}

weapon-component-hammer-ornate-iron = Iron Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-spikedmace-iron = Iron Spiked Mace Head
    .desc = {""}

weapon-component-hammer-hammer-iron = Iron Hammer Head
    .desc = {""}

weapon-component-hammer-warhammer-iron = Iron Warhammer Head
    .desc = {""}

weapon-component-hammer-greathammer-iron = Iron Greathammer Head
    .desc = {""}

weapon-component-hammer-greatmace-iron = Iron Greatmace Head
    .desc = {""}

weapon-component-sword-zweihander-steel = Steel Zweihander Blade
    .desc = {""}

weapon-component-sword-longsword-steel = Steel Longsword Blade
    .desc = {""}

weapon-component-sword-katana-steel = Steel Katana Blade
    .desc = {""}

weapon-component-sword-greatsword-steel = Steel Greatsword Blade
    .desc = {""}

weapon-component-sword-ornate-steel = Steel Ornate Sword Blade
    .desc = {""}

weapon-component-sword-sawblade-steel = Steel Sawblade
    .desc = {""}

weapon-component-sword-sabre-steel = Steel Sabre Blade
    .desc = {""}

weapon-component-staff-grandstaff-hardwood = Hardwood Grandstaff Shaft
    .desc = {""}

weapon-component-staff-rod-hardwood = Hardwood Rod Shaft
    .desc = {""}

weapon-component-staff-brand-hardwood = Hardwood Brand Shaft
    .desc = {""}

weapon-component-staff-longpole-hardwood = Hardwood Long Pole Shaft
    .desc = {""}

weapon-component-staff-staff-hardwood = Hardwood Staff Shaft
    .desc = {""}

weapon-component-staff-ornate-hardwood = Hardwood Ornate Staff Shaft
    .desc = {""}

weapon-component-staff-pole-hardwood = Hardwood Pole Shaft
    .desc = {""}

weapon-component-axe-ornate-orichalcum = Orichalcum Ornate Axe Head
    .desc = {""}

weapon-component-axe-jagged-orichalcum = Orichalcum Jagged Axe Head
    .desc = {""}

weapon-component-axe-axe-orichalcum = Orichalcum Axe Head
    .desc = {""}

weapon-component-axe-battleaxe-orichalcum = Orichalcum Battleaxe Head
    .desc = {""}

weapon-component-axe-greataxe-orichalcum = Orichalcum Greataxe Head
    .desc = {""}

weapon-component-axe-poleaxe-orichalcum = Orichalcum Poleaxe Head
    .desc = {""}

weapon-component-axe-labrys-orichalcum = Orichalcum Labrys Head
    .desc = {""}

weapon-component-bow-greatbow-bamboo = Bamboo Greatbow Limbs
    .desc = {""}

weapon-component-bow-longbow-bamboo = Bamboo Longbow Limbs
    .desc = {""}

weapon-component-bow-composite-bamboo = Bamboo Composite Bow Limbs
    .desc = {""}

weapon-component-bow-ornate-bamboo = Bamboo Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-shortbow-bamboo = Bamboo Shortbow Limbs
    .desc = {""}

weapon-component-bow-bow-bamboo = Bamboo Bow Limbs
    .desc = {""}

weapon-component-bow-warbow-bamboo = Bamboo Warbow Limbs
    .desc = {""}

weapon-component-bow-ornate-frostwood = Frostwood Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-composite-frostwood = Frostwood Composite Bow Limbs
    .desc = {""}

weapon-component-bow-warbow-frostwood = Frostwood Warbow Limbs
    .desc = {""}

weapon-component-bow-shortbow-frostwood = Frostwood Shortbow Limbs
    .desc = {""}

weapon-component-bow-longbow-frostwood = Frostwood Longbow Limbs
    .desc = {""}

weapon-component-bow-bow-frostwood = Frostwood Bow Limbs
    .desc = {""}

weapon-component-bow-greatbow-frostwood = Frostwood Greatbow Limbs
    .desc = {""}

weapon-component-staff-ornate-frostwood = Frostwood Ornate Staff Shaft
    .desc = {""}

weapon-component-staff-pole-frostwood = Frostwood Pole Shaft
    .desc = {""}

weapon-component-staff-longpole-frostwood = Frostwood Long Pole Shaft
    .desc = {""}

weapon-component-staff-grandstaff-frostwood = Frostwood Grandstaff Shaft
    .desc = {""}

weapon-component-staff-rod-frostwood = Frostwood Rod Shaft
    .desc = {""}

weapon-component-staff-staff-frostwood = Frostwood Staff Shaft
    .desc = {""}

weapon-component-staff-brand-frostwood = Frostwood Brand Shaft
    .desc = {""}

weapon-component-bow-greatbow-hardwood = Hardwood Greatbow Limbs
    .desc = {""}

weapon-component-bow-warbow-hardwood = Hardwood Warbow Limbs
    .desc = {""}

weapon-component-bow-shortbow-hardwood = Hardwood Shortbow Limbs
    .desc = {""}

weapon-component-bow-bow-hardwood = Hardwood Bow Limbs
    .desc = {""}

weapon-component-bow-composite-hardwood = Hardwood Composite Bow Limbs
    .desc = {""}

weapon-component-bow-ornate-hardwood = Hardwood Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-longbow-hardwood = Hardwood Longbow Limbs
    .desc = {""}

weapon-component-sceptre-cane-ironwood = Ironwood Cane Shaft
    .desc = {""}

weapon-component-sceptre-ornate-ironwood = Ironwood Ornate Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-grandsceptre-ironwood = Ironwood Grandsceptre Shaft
    .desc = {""}

weapon-component-sceptre-arbor-ironwood = Ironwood Arbor Shaft
    .desc = {""}

weapon-component-sceptre-crozier-ironwood = Ironwood Crozier Shaft
    .desc = {""}

weapon-component-sceptre-crook-ironwood = Ironwood Crook Shaft
    .desc = {""}

weapon-component-sceptre-sceptre-ironwood = Ironwood Sceptre Shaft
    .desc = {""}

weapon-component-axe-greataxe-iron = Iron Greataxe Head
    .desc = {""}

weapon-component-axe-jagged-iron = Iron Jagged Axe Head
    .desc = {""}

weapon-component-axe-battleaxe-iron = Iron Battleaxe Head
    .desc = {""}

weapon-component-axe-labrys-iron = Iron Labrys Head
    .desc = {""}

weapon-component-axe-axe-iron = Iron Axe Head
    .desc = {""}

weapon-component-axe-ornate-iron = Iron Ornate Axe Head
    .desc = {""}

weapon-component-axe-poleaxe-iron = Iron Poleaxe Head
    .desc = {""}

weapon-component-axe-greataxe-steel = Steel Greataxe Head
    .desc = {""}

weapon-component-axe-ornate-steel = Steel Ornate Axe Head
    .desc = {""}

weapon-component-axe-jagged-steel = Steel Jagged Axe Head
    .desc = {""}

weapon-component-axe-battleaxe-steel = Steel Battleaxe Head
    .desc = {""}

weapon-component-axe-poleaxe-steel = Steel Poleaxe Head
    .desc = {""}

weapon-component-axe-labrys-steel = Steel Labrys Head
    .desc = {""}

weapon-component-axe-axe-steel = Steel Axe Head
    .desc = {""}

weapon-component-sword-longsword-bronze = Bronze Longsword Blade
    .desc = {""}

weapon-component-sword-zweihander-bronze = Bronze Zweihander Blade
    .desc = {""}

weapon-component-sword-greatsword-bronze = Bronze Greatsword Blade
    .desc = {""}

weapon-component-sword-katana-bronze = Bronze Katana Blade
    .desc = {""}

weapon-component-sword-sabre-bronze = Bronze Sabre Blade
    .desc = {""}

weapon-component-sword-ornate-bronze = Bronze Ornate Sword Blade
    .desc = {""}

weapon-component-sword-sawblade-bronze = Bronze Sawblade
    .desc = {""}

weapon-component-sceptre-crozier-wood = Wooden Crozier Shaft
    .desc = {""}

weapon-component-sceptre-sceptre-wood = Wooden Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-cane-wood = Wooden Cane Shaft
    .desc = {""}

weapon-component-sceptre-ornate-wood = Wooden Ornate Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-arbor-wood = Wooden Arbor Shaft
    .desc = {""}

weapon-component-sceptre-crook-wood = Wooden Crook Shaft
    .desc = {""}

weapon-component-sceptre-grandsceptre-wood = Wooden Grandsceptre Shaft
    .desc = {""}

weapon-component-staff-brand-bamboo = Bamboo Brand Shaft
    .desc = {""}

weapon-component-staff-grandstaff-bamboo = Bamboo Grandstaff Shaft
    .desc = {""}

weapon-component-staff-pole-bamboo = Bamboo Pole Shaft
    .desc = {""}

weapon-component-staff-staff-bamboo = Bamboo Staff Shaft
    .desc = {""}

weapon-component-staff-rod-bamboo = Bamboo Rod Shaft
    .desc = {""}

weapon-component-staff-longpole-bamboo = Bamboo Long Pole Shaft
    .desc = {""}

weapon-component-staff-ornate-bamboo = Bamboo Ornate Staff Shaft
    .desc = {""}

weapon-component-hammer-warhammer-orichalcum = Orichalcum Warhammer Head
    .desc = {""}

weapon-component-hammer-greathammer-orichalcum = Orichalcum Greathammer Head
    .desc = {""}

weapon-component-hammer-greatmace-orichalcum = Orichalcum Greatmace Head
    .desc = {""}

weapon-component-hammer-hammer-orichalcum = Orichalcum Hammer Head
    .desc = {""}

weapon-component-hammer-spikedmace-orichalcum = Orichalcum Spiked Mace Head
    .desc = {""}

weapon-component-hammer-ornate-orichalcum = Orichalcum Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-maul-orichalcum = Orichalcum Maul Head
    .desc = {""}

weapon-component-bow-shortbow-ironwood = Ironwood Shortbow Limbs
    .desc = {""}

weapon-component-bow-greatbow-ironwood = Ironwood Greatbow Limbs
    .desc = {""}

weapon-component-bow-ornate-ironwood = Ironwood Ornate Bow Limbs
    .desc = {""}

weapon-component-bow-longbow-ironwood = Ironwood Longbow Limbs
    .desc = {""}

weapon-component-bow-warbow-ironwood = Ironwood Warbow Limbs
    .desc = {""}

weapon-component-bow-composite-ironwood = Ironwood Composite Bow Limbs
    .desc = {""}

weapon-component-bow-bow-ironwood = Ironwood Bow Limbs
    .desc = {""}

weapon-component-hammer-ornate-cobalt = Cobalt Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-hammer-cobalt = Cobalt Hammer Head
    .desc = {""}

weapon-component-hammer-greatmace-cobalt = Cobalt Greatmace Head
    .desc = {""}

weapon-component-hammer-spikedmace-cobalt = Cobalt Spiked Mace Head
    .desc = {""}

weapon-component-hammer-greathammer-cobalt = Cobalt Greathammer Head
    .desc = {""}

weapon-component-hammer-maul-cobalt = Cobalt Maul Head
    .desc = {""}

weapon-component-hammer-warhammer-cobalt = Cobalt Warhammer Head
    .desc = {""}

weapon-component-staff-ornate-wood = Wooden Ornate Staff Shaft
    .desc = {""}

weapon-component-staff-pole-wood = Wooden Pole Shaft
    .desc = {""}

weapon-component-staff-rod-wood = Wooden Rod Shaft
    .desc = {""}

weapon-component-staff-brand-wood = Wooden Brand Shaft
    .desc = {""}

weapon-component-staff-grandstaff-wood = Wooden Grandstaff Shaft
    .desc = {""}

weapon-component-staff-staff-wood = Wooden Staff Shaft
    .desc = {""}

weapon-component-staff-longpole-wood = Wooden Long Pole Shaft
    .desc = {""}

weapon-component-hammer-ornate-steel = Steel Ornate Hammer Head
    .desc = {""}

weapon-component-hammer-hammer-steel = Steel Hammer Head
    .desc = {""}

weapon-component-hammer-greathammer-steel = Steel Greathammer Head
    .desc = {""}

weapon-component-hammer-spikedmace-steel = Steel Spiked Mace Head
    .desc = {""}

weapon-component-hammer-greatmace-steel = Steel Greatmace Head
    .desc = {""}

weapon-component-hammer-maul-steel = Steel Maul Head
    .desc = {""}

weapon-component-hammer-warhammer-steel = Steel Warhammer Head
    .desc = {""}

weapon-component-sceptre-grandsceptre-bamboo = Bamboo Grandsceptre Shaft
    .desc = {""}

weapon-component-sceptre-cane-bamboo = Bamboo Cane Shaft
    .desc = {""}

weapon-component-sceptre-crozier-bamboo = Bamboo Crozier Shaft
    .desc = {""}

weapon-component-sceptre-crook-bamboo = Bamboo Crook Shaft
    .desc = {""}

weapon-component-sceptre-sceptre-bamboo = Bamboo Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-arbor-bamboo = Bamboo Arbor Shaft
    .desc = {""}

weapon-component-sceptre-ornate-bamboo = Bamboo Ornate Sceptre Shaft
    .desc = {""}

weapon-component-staff-grandstaff-ironwood = Ironwood Grandstaff Shaft
    .desc = {""}

weapon-component-staff-rod-ironwood = Ironwood Rod Shaft
    .desc = {""}

weapon-component-staff-brand-ironwood = Ironwood Brand Shaft
    .desc = {""}

weapon-component-staff-longpole-ironwood = Ironwood Long Pole Shaft
    .desc = {""}

weapon-component-staff-staff-ironwood = Ironwood Staff Shaft
    .desc = {""}

weapon-component-staff-pole-ironwood = Ironwood Pole Shaft
    .desc = {""}

weapon-component-staff-ornate-ironwood = Ironwood Ornate Staff Shaft
    .desc = {""}

weapon-component-staff-ornate-eldwood = Eldwood Ornate Staff Shaft
    .desc = {""}

weapon-component-staff-longpole-eldwood = Eldwood Long Pole Shaft
    .desc = {""}

weapon-component-staff-rod-eldwood = Eldwood Rod Shaft
    .desc = {""}

weapon-component-staff-pole-eldwood = Eldwood Pole Shaft
    .desc = {""}

weapon-component-staff-staff-eldwood = Eldwood Staff Shaft
    .desc = {""}

weapon-component-staff-brand-eldwood = Eldwood Brand Shaft
    .desc = {""}

weapon-component-staff-grandstaff-eldwood = Eldwood Grandstaff Shaft
    .desc = {""}

weapon-component-sceptre-arbor-eldwood = Eldwood Arbor Shaft
    .desc = {""}

weapon-component-sceptre-crook-eldwood = Eldwood Crook Shaft
    .desc = {""}

weapon-component-sceptre-crozier-eldwood = Eldwood Crozier Shaft
    .desc = {""}

weapon-component-sceptre-cane-eldwood = Eldwood Cane Shaft
    .desc = {""}

weapon-component-sceptre-grandsceptre-eldwood = Eldwood Grandsceptre Shaft
    .desc = {""}

weapon-component-sceptre-ornate-eldwood = Eldwood Ornate Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-sceptre-eldwood = Eldwood Sceptre Shaft
    .desc = {""}

weapon-component-sword-sabre-bloodsteel = Bloodsteel Sabre Blade
    .desc = {""}

weapon-component-sword-zweihander-bloodsteel = Bloodsteel Zweihander Blade
    .desc = {""}

weapon-component-sword-greatsword-bloodsteel = Bloodsteel Greatsword Blade
    .desc = {""}

weapon-component-sword-katana-bloodsteel = Bloodsteel Katana Blade
    .desc = {""}

weapon-component-sword-longsword-bloodsteel = Bloodsteel Longsword Blade
    .desc = {""}

weapon-component-sword-ornate-bloodsteel = Bloodsteel Ornate Sword Blade
    .desc = {""}

weapon-component-sword-sawblade-bloodsteel = Bloodsteel Sawblade
    .desc = {""}

weapon-component-sword-sabre-orichalcum = Orichalcum Sabre Blade
    .desc = {""}

weapon-component-sword-greatsword-orichalcum = Orichalcum Greatsword Blade
    .desc = {""}

weapon-component-sword-sawblade-orichalcum = Orichalcum Sawblade
    .desc = {""}

weapon-component-sword-longsword-orichalcum = Orichalcum Longsword Blade
    .desc = {""}

weapon-component-sword-katana-orichalcum = Orichalcum Katana Blade
    .desc = {""}

weapon-component-sword-zweihander-orichalcum = Orichalcum Zweihander Blade
    .desc = {""}

weapon-component-sword-ornate-orichalcum = Orichalcum Ornate Sword Blade
    .desc = {""}

weapon-component-axe-poleaxe-bronze = Bronze Poleaxe Head
    .desc = {""}

weapon-component-axe-ornate-bronze = Bronze Ornate Axe Head
    .desc = {""}

weapon-component-axe-labrys-bronze = Bronze Labrys Head
    .desc = {""}

weapon-component-axe-battleaxe-bronze = Bronze Battleaxe Head
    .desc = {""}

weapon-component-axe-axe-bronze = Bronze Axe Head
    .desc = {""}

weapon-component-axe-jagged-bronze = Bronze Jagged Axe Head
    .desc = {""}

weapon-component-axe-greataxe-bronze = Bronze Greataxe Head
    .desc = {""}

weapon-component-sceptre-sceptre-hardwood = Hardwood Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-cane-hardwood = Hardwood Cane Shaft
    .desc = {""}

weapon-component-sceptre-arbor-hardwood = Hardwood Arbor Shaft
    .desc = {""}

weapon-component-sceptre-crozier-hardwood = Hardwood Crozier Shaft
    .desc = {""}

weapon-component-sceptre-ornate-hardwood = Hardwood Ornate Sceptre Shaft
    .desc = {""}

weapon-component-sceptre-crook-hardwood = Hardwood Crook Shaft
    .desc = {""}

weapon-component-sceptre-grandsceptre-hardwood = Hardwood Grandsceptre Shaft
    .desc = {""}

weapon-component-sword-longsword-cobalt = Cobalt Longsword Blade
    .desc = {""}

weapon-component-sword-sawblade-cobalt = Cobalt Sawblade
    .desc = {""}

weapon-component-sword-greatsword-cobalt = Cobalt Greatsword Blade
    .desc = {""}

weapon-component-sword-katana-cobalt = Cobalt Katana Blade
    .desc = {""}

weapon-component-sword-zweihander-cobalt = Cobalt Zweihander Blade
    .desc = {""}

weapon-component-sword-ornate-cobalt = Cobalt Ornate Sword Blade
    .desc = {""}

weapon-component-sword-sabre-cobalt = Cobalt Sabre Blade
    .desc = {""}

weapon-sword-greatsword-iron-2h = Iron Greatsword
    .desc = {""}

weapon-sword-katana-iron-2h = Iron Katana
    .desc = {""}

weapon-sword-katana-iron-1h = Iron Swiftblade
    .desc = {""}

weapon-sword-sawblade-iron-2h = Iron Sawblade
    .desc = {""}

weapon-sword-sawblade-iron-1h = Iron Sawback
    .desc = {""}

weapon-sword-sabre-iron-2h = Iron Sabre
    .desc = {""}

weapon-sword-sabre-iron-1h = Iron Scimitar
    .desc = {""}

weapon-sword-ornate-iron-2h = Iron Ornate Sword
    .desc = {""}

weapon-sword-ornate-iron-1h = Iron Rapier
    .desc = {""}

weapon-sword-longsword-iron-2h = Iron Longsword
    .desc = {""}

weapon-sword-longsword-iron-1h = Iron Shortsword
    .desc = {""}

weapon-sword-zweihander-iron-2h = Iron Zweihander
    .desc = {""}

weapon-bow-shortbow-wood = Wooden Shortbow
    .desc = {""}

weapon-bow-longbow-wood = Wooden Longbow
    .desc = {""}

weapon-bow-ornate-wood = Wooden Ornate Bow
    .desc = {""}

weapon-bow-greatbow-wood = Wooden Greatbow
    .desc = {""}

weapon-bow-composite-wood = Wooden Composite Bow
    .desc = {""}

weapon-bow-bow-wood = Wooden Bow
    .desc = {""}

weapon-bow-warbow-wood = Wooden Warbow
    .desc = {""}

weapon-hammer-maul-bronze-2h = Bronze Maul
    .desc = {""}

weapon-hammer-hammer-bronze-2h = Bronze Hammer
    .desc = {""}

weapon-hammer-hammer-bronze-1h = Bronze Club
    .desc = {""}

weapon-hammer-greatmace-bronze-2h = Bronze Greatmace
    .desc = {""}

weapon-hammer-spikedmace-bronze-2h = Bronze Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-bronze-1h = Bronze Mace
    .desc = {""}

weapon-hammer-greathammer-bronze-2h = Bronze Greathammer
    .desc = {""}

weapon-hammer-warhammer-bronze-2h = Bronze Warhammer
    .desc = {""}

weapon-hammer-warhammer-bronze-1h = Bronze Mallet
    .desc = {""}

weapon-hammer-ornate-bronze-2h = Bronze Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-bronze-1h = Bronze Cudgel
    .desc = {""}

weapon-hammer-maul-bloodsteel-2h = Bloodsteel Maul
    .desc = {""}

weapon-hammer-greathammer-bloodsteel-2h = Bloodsteel Greathammer
    .desc = {""}

weapon-hammer-ornate-bloodsteel-2h = Bloodsteel Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-bloodsteel-1h = Bloodsteel Cudgel
    .desc = {""}

weapon-hammer-hammer-bloodsteel-2h = Bloodsteel Hammer
    .desc = {""}

weapon-hammer-hammer-bloodsteel-1h = Bloodsteel Club
    .desc = {""}

weapon-hammer-spikedmace-bloodsteel-2h = Bloodsteel Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-bloodsteel-1h = Bloodsteel Mace
    .desc = {""}

weapon-hammer-greatmace-bloodsteel-2h = Bloodsteel Greatmace
    .desc = {""}

weapon-hammer-warhammer-bloodsteel-2h = Bloodsteel Warhammer
    .desc = {""}

weapon-hammer-warhammer-bloodsteel-1h = Bloodsteel Mallet
    .desc = {""}

weapon-sceptre-ornate-frostwood = Frostwood Ornate Sceptre
    .desc = {""}

weapon-sceptre-sceptre-frostwood = Frostwood Sceptre
    .desc = {""}

weapon-sceptre-crook-frostwood = Frostwood Crook
    .desc = {""}

weapon-sceptre-grandsceptre-frostwood = Frostwood Grandsceptre
    .desc = {""}

weapon-sceptre-cane-frostwood = Frostwood Cane
    .desc = {""}

weapon-sceptre-crozier-frostwood = Frostwood Crozier
    .desc = {""}

weapon-sceptre-arbor-frostwood = Frostwood Arbor
    .desc = {""}

weapon-bow-bow-eldwood = Eldwood Bow
    .desc = {""}

weapon-bow-warbow-eldwood = Eldwood Warbow
    .desc = {""}

weapon-bow-greatbow-eldwood = Eldwood Greatbow
    .desc = {""}

weapon-bow-ornate-eldwood = Eldwood Ornate Bow
    .desc = {""}

weapon-bow-longbow-eldwood = Eldwood Longbow
    .desc = {""}

weapon-bow-composite-eldwood = Eldwood Composite Bow
    .desc = {""}

weapon-bow-shortbow-eldwood = Eldwood Shortbow
    .desc = {""}

weapon-axe-axe-bloodsteel-2h = Bloodsteel Axe
    .desc = {""}

weapon-axe-axe-bloodsteel-1h = Bloodsteel Hatchet
    .desc = {""}

weapon-axe-ornate-bloodsteel-2h = Bloodsteel Ornate Axe
    .desc = {""}

weapon-axe-ornate-bloodsteel-1h = Bloodsteel Kilonda
    .desc = {""}

weapon-axe-jagged-bloodsteel-2h = Bloodsteel Jagged Axe
    .desc = {""}

weapon-axe-jagged-bloodsteel-1h = Bloodsteel Tomahawk
    .desc = {""}

weapon-axe-battleaxe-bloodsteel-2h = Bloodsteel Battleaxe
    .desc = {""}

weapon-axe-battleaxe-bloodsteel-1h = Bloodsteel Cleaver
    .desc = {""}

weapon-axe-greataxe-bloodsteel-2h = Bloodsteel Greataxe
    .desc = {""}

weapon-axe-labrys-bloodsteel-2h = Bloodsteel Labrys
    .desc = {""}

weapon-axe-poleaxe-bloodsteel-2h = Bloodsteel Poleaxe
    .desc = {""}

weapon-axe-poleaxe-cobalt-2h = Cobalt Poleaxe
    .desc = {""}

weapon-axe-jagged-cobalt-2h = Cobalt Jagged Axe
    .desc = {""}

weapon-axe-jagged-cobalt-1h = Cobalt Tomahawk
    .desc = {""}

weapon-axe-labrys-cobalt-2h = Cobalt Labrys
    .desc = {""}

weapon-axe-axe-cobalt-2h = Cobalt Axe
    .desc = {""}

weapon-axe-axe-cobalt-1h = Cobalt Hatchet
    .desc = {""}

weapon-axe-ornate-cobalt-2h = Cobalt Ornate Axe
    .desc = {""}

weapon-axe-ornate-cobalt-1h = Cobalt Kilonda
    .desc = {""}

weapon-axe-greataxe-cobalt-2h = Cobalt Greataxe
    .desc = {""}

weapon-axe-battleaxe-cobalt-2h = Cobalt Battleaxe
    .desc = {""}

weapon-axe-battleaxe-cobalt-1h = Cobalt Cleaver
    .desc = {""}

weapon-hammer-maul-iron-2h = Iron Maul
    .desc = {""}

weapon-hammer-ornate-iron-2h = Iron Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-iron-1h = Iron Cudgel
    .desc = {""}

weapon-hammer-spikedmace-iron-2h = Iron Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-iron-1h = Iron Mace
    .desc = {""}

weapon-hammer-hammer-iron-2h = Iron Hammer
    .desc = {""}

weapon-hammer-hammer-iron-1h = Iron Club
    .desc = {""}

weapon-hammer-warhammer-iron-2h = Iron Warhammer
    .desc = {""}

weapon-hammer-warhammer-iron-1h = Iron Mallet
    .desc = {""}

weapon-hammer-greathammer-iron-2h = Iron Greathammer
    .desc = {""}

weapon-hammer-greatmace-iron-2h = Iron Greatmace
    .desc = {""}

weapon-sword-zweihander-steel-2h = Steel Zweihander
    .desc = {""}

weapon-sword-longsword-steel-2h = Steel Longsword
    .desc = {""}

weapon-sword-longsword-steel-1h = Steel Shortsword
    .desc = {""}

weapon-sword-katana-steel-2h = Steel Katana
    .desc = {""}

weapon-sword-katana-steel-1h = Steel Swiftblade
    .desc = {""}

weapon-sword-greatsword-steel-2h = Steel Greatsword
    .desc = {""}

weapon-sword-ornate-steel-2h = Steel Ornate Sword
    .desc = {""}

weapon-sword-ornate-steel-1h = Steel Rapier
    .desc = {""}

weapon-sword-sawblade-steel-2h = Steel Sawblade
    .desc = {""}

weapon-sword-sawblade-steel-1h = Steel Sawback
    .desc = {""}

weapon-sword-sabre-steel-2h = Steel Sabre
    .desc = {""}

weapon-sword-sabre-steel-1h = Steel Scimitar
    .desc = {""}

weapon-staff-grandstaff-hardwood = Hardwood Grandstaff
    .desc = {""}

weapon-staff-rod-hardwood = Hardwood Rod
    .desc = {""}

weapon-staff-brand-hardwood = Hardwood Brand
    .desc = {""}

weapon-staff-longpole-hardwood = Hardwood Longpole
    .desc = {""}

weapon-staff-staff-hardwood = Hardwood Staff
    .desc = {""}

weapon-staff-ornate-hardwood = Hardwood Ornate Staff
    .desc = {""}

weapon-staff-pole-hardwood = Hardwood Pole
    .desc = {""}

weapon-axe-ornate-orichalcum-2h = Orichalcum Ornate Axe
    .desc = {""}

weapon-axe-ornate-orichalcum-1h = Orichalcum Kilonda
    .desc = {""}

weapon-axe-jagged-orichalcum-2h = Orichalcum Jagged Axe
    .desc = {""}

weapon-axe-jagged-orichalcum-1h = Orichalcum Tomahawk
    .desc = {""}

weapon-axe-axe-orichalcum-2h = Orichalcum Axe
    .desc = {""}

weapon-axe-axe-orichalcum-1h = Orichalcum Hatchet
    .desc = {""}

weapon-axe-battleaxe-orichalcum-2h = Orichalcum Battleaxe
    .desc = {""}

weapon-axe-battleaxe-orichalcum-1h = Orichalcum Cleaver
    .desc = {""}

weapon-axe-greataxe-orichalcum-2h = Orichalcum Greataxe
    .desc = {""}

weapon-axe-poleaxe-orichalcum-2h = Orichalcum Poleaxe
    .desc = {""}

weapon-axe-labrys-orichalcum-2h = Orichalcum Labrys
    .desc = {""}

weapon-bow-greatbow-bamboo = Bamboo Greatbow
    .desc = {""}

weapon-bow-longbow-bamboo = Bamboo Longbow
    .desc = {""}

weapon-bow-composite-bamboo = Bamboo Composite Bow
    .desc = {""}

weapon-bow-ornate-bamboo = Bamboo Ornate Bow
    .desc = {""}

weapon-bow-shortbow-bamboo = Bamboo Shortbow
    .desc = {""}

weapon-bow-bow-bamboo = Bamboo Bow
    .desc = {""}

weapon-bow-warbow-bamboo = Bamboo Warbow
    .desc = {""}

weapon-bow-ornate-frostwood = Frostwood Ornate Bow
    .desc = {""}

weapon-bow-composite-frostwood = Frostwood Composite Bow
    .desc = {""}

weapon-bow-warbow-frostwood = Frostwood Warbow
    .desc = {""}

weapon-bow-shortbow-frostwood = Frostwood Shortbow
    .desc = {""}

weapon-bow-longbow-frostwood = Frostwood Longbow
    .desc = {""}

weapon-bow-bow-frostwood = Frostwood Bow
    .desc = {""}

weapon-bow-greatbow-frostwood = Frostwood Greatbow
    .desc = {""}

weapon-staff-ornate-frostwood = Frostwood Ornate Staff
    .desc = {""}

weapon-staff-pole-frostwood = Frostwood Pole
    .desc = {""}

weapon-staff-longpole-frostwood = Frostwood Longpole
    .desc = {""}

weapon-staff-grandstaff-frostwood = Frostwood Grandstaff
    .desc = {""}

weapon-staff-rod-frostwood = Frostwood Rod
    .desc = {""}

weapon-staff-staff-frostwood = Frostwood Staff
    .desc = {""}

weapon-staff-brand-frostwood = Frostwood Brand
    .desc = {""}

weapon-bow-greatbow-hardwood = Hardwood Greatbow
    .desc = {""}

weapon-bow-warbow-hardwood = Hardwood Warbow
    .desc = {""}

weapon-bow-shortbow-hardwood = Hardwood Shortbow
    .desc = {""}

weapon-bow-bow-hardwood = Hardwood Bow
    .desc = {""}

weapon-bow-composite-hardwood = Hardwood Composite Bow
    .desc = {""}

weapon-bow-ornate-hardwood = Hardwood Ornate Bow
    .desc = {""}

weapon-bow-longbow-hardwood = Hardwood Longbow
    .desc = {""}

weapon-sceptre-cane-ironwood = Ironwood Cane
    .desc = {""}

weapon-sceptre-ornate-ironwood = Ironwood Ornate Sceptre
    .desc = {""}

weapon-sceptre-grandsceptre-ironwood = Ironwood Grandsceptre
    .desc = {""}

weapon-sceptre-arbor-ironwood = Ironwood Arbor
    .desc = {""}

weapon-sceptre-crozier-ironwood = Ironwood Crozier
    .desc = {""}

weapon-sceptre-crook-ironwood = Ironwood Crook
    .desc = {""}

weapon-sceptre-sceptre-ironwood = Ironwood Sceptre
    .desc = {""}

weapon-axe-greataxe-iron-2h = Iron Greataxe
    .desc = {""}

weapon-axe-jagged-iron-2h = Iron Jagged Axe
    .desc = {""}

weapon-axe-jagged-iron-1h = Iron Tomahawk
    .desc = {""}

weapon-axe-battleaxe-iron-2h = Iron Battleaxe
    .desc = {""}

weapon-axe-battleaxe-iron-1h = Iron Cleaver
    .desc = {""}

weapon-axe-labrys-iron-2h = Iron Labrys
    .desc = {""}

weapon-axe-axe-iron-2h = Iron Axe
    .desc = {""}

weapon-axe-axe-iron-1h = Iron Hatchet
    .desc = {""}

weapon-axe-ornate-iron-2h = Iron Ornate Axe
    .desc = {""}

weapon-axe-ornate-iron-1h = Iron Kilonda
    .desc = {""}

weapon-axe-poleaxe-iron-2h = Iron Poleaxe
    .desc = {""}

weapon-axe-greataxe-steel-2h = Steel Greataxe
    .desc = {""}

weapon-axe-ornate-steel-2h = Steel Ornate Axe
    .desc = {""}

weapon-axe-ornate-steel-1h = Steel Kilonda
    .desc = {""}

weapon-axe-jagged-steel-2h = Steel Jagged Axe
    .desc = {""}

weapon-axe-jagged-steel-1h = Steel Tomahawk
    .desc = {""}

weapon-axe-battleaxe-steel-2h = Steel Battleaxe
    .desc = {""}

weapon-axe-battleaxe-steel-1h = Steel Cleaver
    .desc = {""}

weapon-axe-poleaxe-steel-2h = Steel Poleaxe
    .desc = {""}

weapon-axe-labrys-steel-2h = Steel Labrys
    .desc = {""}

weapon-axe-axe-steel-2h = Steel Axe
    .desc = {""}

weapon-axe-axe-steel-1h = Steel Hatchet
    .desc = {""}

weapon-sword-longsword-bronze-2h = Bronze Longsword
    .desc = {""}

weapon-sword-longsword-bronze-1h = Bronze Shortsword
    .desc = {""}

weapon-sword-zweihander-bronze-2h = Bronze Zweihander
    .desc = {""}

weapon-sword-greatsword-bronze-2h = Bronze Greatsword
    .desc = {""}

weapon-sword-katana-bronze-2h = Bronze Katana
    .desc = {""}

weapon-sword-katana-bronze-1h = Bronze Swiftblade
    .desc = {""}

weapon-sword-sabre-bronze-2h = Bronze Sabre
    .desc = {""}

weapon-sword-sabre-bronze-1h = Bronze Scimitar
    .desc = {""}

weapon-sword-ornate-bronze-2h = Bronze Ornate Sword
    .desc = {""}

weapon-sword-ornate-bronze-1h = Bronze Rapier
    .desc = {""}

weapon-sword-sawblade-bronze-2h = Bronze Sawblade
    .desc = {""}

weapon-sword-sawblade-bronze-1h = Bronze Sawback
    .desc = {""}

weapon-sceptre-crozier-wood = Wooden Crozier
    .desc = {""}

weapon-sceptre-sceptre-wood = Wooden Sceptre
    .desc = {""}

weapon-sceptre-cane-wood = Wooden Cane
    .desc = {""}

weapon-sceptre-ornate-wood = Wooden Ornate Sceptre
    .desc = {""}

weapon-sceptre-arbor-wood = Wooden Arbor
    .desc = {""}

weapon-sceptre-crook-wood = Wooden Crook
    .desc = {""}

weapon-sceptre-grandsceptre-wood = Wooden Grandsceptre
    .desc = {""}

weapon-staff-brand-bamboo = Bamboo Brand
    .desc = {""}

weapon-staff-grandstaff-bamboo = Bamboo Grandstaff
    .desc = {""}

weapon-staff-pole-bamboo = Bamboo Pole
    .desc = {""}

weapon-staff-staff-bamboo = Bamboo Staff
    .desc = {""}

weapon-staff-rod-bamboo = Bamboo Rod
    .desc = {""}

weapon-staff-longpole-bamboo = Bamboo Longpole
    .desc = {""}

weapon-staff-ornate-bamboo = Bamboo Ornate Staff
    .desc = {""}

weapon-hammer-warhammer-orichalcum-2h = Orichalcum Warhammer
    .desc = {""}

weapon-hammer-warhammer-orichalcum-1h = Orichalcum Mallet
    .desc = {""}

weapon-hammer-greathammer-orichalcum-2h = Orichalcum Greathammer
    .desc = {""}

weapon-hammer-greatmace-orichalcum-2h = Orichalcum Greatmace
    .desc = {""}

weapon-hammer-hammer-orichalcum-2h = Orichalcum Hammer
    .desc = {""}

weapon-hammer-hammer-orichalcum-1h = Orichalcum Club
    .desc = {""}

weapon-hammer-spikedmace-orichalcum-2h = Orichalcum Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-orichalcum-1h = Orichalcum Mace
    .desc = {""}

weapon-hammer-ornate-orichalcum-2h = Orichalcum Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-orichalcum-1h = Orichalcum Cudgel
    .desc = {""}

weapon-hammer-maul-orichalcum-2h = Orichalcum Maul
    .desc = {""}

weapon-bow-shortbow-ironwood = Ironwood Shortbow
    .desc = {""}

weapon-bow-greatbow-ironwood = Ironwood Greatbow
    .desc = {""}

weapon-bow-ornate-ironwood = Ironwood Ornate Bow
    .desc = {""}

weapon-bow-longbow-ironwood = Ironwood Longbow
    .desc = {""}

weapon-bow-warbow-ironwood = Ironwood Warbow
    .desc = {""}

weapon-bow-composite-ironwood = Ironwood Composite Bow
    .desc = {""}

weapon-bow-bow-ironwood = Ironwood Bow
    .desc = {""}

weapon-hammer-ornate-cobalt-2h = Cobalt Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-cobalt-1h = Cobalt Cudgel
    .desc = {""}

weapon-hammer-hammer-cobalt-2h = Cobalt Hammer
    .desc = {""}

weapon-hammer-hammer-cobalt-1h = Cobalt Club
    .desc = {""}

weapon-hammer-greatmace-cobalt-2h = Cobalt Greatmace
    .desc = {""}

weapon-hammer-spikedmace-cobalt-2h = Cobalt Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-cobalt-1h = Cobalt Mace
    .desc = {""}

weapon-hammer-greathammer-cobalt-2h = Cobalt Greathammer
    .desc = {""}

weapon-hammer-maul-cobalt-2h = Cobalt Maul
    .desc = {""}

weapon-hammer-warhammer-cobalt-2h = Cobalt Warhammer
    .desc = {""}

weapon-hammer-warhammer-cobalt-1h = Cobalt Mallet
    .desc = {""}

weapon-staff-ornate-wood = Wooden Ornate Staff
    .desc = {""}

weapon-staff-pole-wood = Wooden Pole
    .desc = {""}

weapon-staff-rod-wood = Wooden Rod
    .desc = {""}

weapon-staff-brand-wood = Wooden Brand
    .desc = {""}

weapon-staff-grandstaff-wood = Wooden Grandstaff
    .desc = {""}

weapon-staff-staff-wood = Wooden Staff
    .desc = {""}

weapon-staff-longpole-wood = Wooden Longpole
    .desc = {""}

weapon-hammer-ornate-steel-2h = Steel Ornate Hammer
    .desc = {""}

weapon-hammer-ornate-steel-1h = Steel Cudgel
    .desc = {""}

weapon-hammer-hammer-steel-2h = Steel Hammer
    .desc = {""}

weapon-hammer-hammer-steel-1h = Steel Club
    .desc = {""}

weapon-hammer-greathammer-steel-2h = Steel Greathammer
    .desc = {""}

weapon-hammer-spikedmace-steel-2h = Steel Spiked Mace
    .desc = {""}

weapon-hammer-spikedmace-steel-1h = Steel Mace
    .desc = {""}

weapon-hammer-greatmace-steel-2h = Steel Greatmace
    .desc = {""}

weapon-hammer-maul-steel-2h = Steel Maul
    .desc = {""}

weapon-hammer-warhammer-steel-2h = Steel Warhammer
    .desc = {""}

weapon-hammer-warhammer-steel-1h = Steel Mallet
    .desc = {""}

weapon-sceptre-grandsceptre-bamboo = Bamboo Grandsceptre
    .desc = {""}

weapon-sceptre-cane-bamboo = Bamboo Cane
    .desc = {""}

weapon-sceptre-crozier-bamboo = Bamboo Crozier
    .desc = {""}

weapon-sceptre-crook-bamboo = Bamboo Crook
    .desc = {""}

weapon-sceptre-sceptre-bamboo = Bamboo Sceptre
    .desc = {""}

weapon-sceptre-arbor-bamboo = Bamboo Arbor
    .desc = {""}

weapon-sceptre-ornate-bamboo = Bamboo Ornate Sceptre
    .desc = {""}

weapon-staff-grandstaff-ironwood = Ironwood Grandstaff
    .desc = {""}

weapon-staff-rod-ironwood = Ironwood Rod
    .desc = {""}

weapon-staff-brand-ironwood = Ironwood Brand
    .desc = {""}

weapon-staff-longpole-ironwood = Ironwood Longpole
    .desc = {""}

weapon-staff-staff-ironwood = Ironwood Staff
    .desc = {""}

weapon-staff-pole-ironwood = Ironwood Pole
    .desc = {""}

weapon-staff-ornate-ironwood = Ironwood Ornate Staff
    .desc = {""}

weapon-staff-ornate-eldwood = Eldwood Ornate Staff
    .desc = {""}

weapon-staff-longpole-eldwood = Eldwood Longpole
    .desc = {""}

weapon-staff-rod-eldwood = Eldwood Rod
    .desc = {""}

weapon-staff-pole-eldwood = Eldwood Pole
    .desc = {""}

weapon-staff-staff-eldwood = Eldwood Staff
    .desc = {""}

weapon-staff-brand-eldwood = Eldwood Brand
    .desc = {""}

weapon-staff-grandstaff-eldwood = Eldwood Grandstaff
    .desc = {""}

weapon-sceptre-arbor-eldwood = Eldwood Arbor
    .desc = {""}

weapon-sceptre-crook-eldwood = Eldwood Crook
    .desc = {""}

weapon-sceptre-crozier-eldwood = Eldwood Crozier
    .desc = {""}

weapon-sceptre-cane-eldwood = Eldwood Cane
    .desc = {""}

weapon-sceptre-grandsceptre-eldwood = Eldwood Grandsceptre
    .desc = {""}

weapon-sceptre-ornate-eldwood = Eldwood Ornate Sceptre
    .desc = {""}

weapon-sceptre-sceptre-eldwood = Eldwood Sceptre
    .desc = {""}

weapon-sword-sabre-bloodsteel-2h = Bloodsteel Sabre
    .desc = {""}

weapon-sword-sabre-bloodsteel-1h = Bloodsteel Scimitar
    .desc = {""}

weapon-sword-zweihander-bloodsteel-2h = Bloodsteel Zweihander
    .desc = {""}

weapon-sword-greatsword-bloodsteel-2h = Bloodsteel Greatsword
    .desc = {""}

weapon-sword-katana-bloodsteel-2h = Bloodsteel Katana
    .desc = {""}

weapon-sword-katana-bloodsteel-1h = Bloodsteel Swiftblade
    .desc = {""}

weapon-sword-longsword-bloodsteel-2h = Bloodsteel Longsword
    .desc = {""}

weapon-sword-longsword-bloodsteel-1h = Bloodsteel Shortsword
    .desc = {""}

weapon-sword-ornate-bloodsteel-2h = Bloodsteel Ornate Sword
    .desc = {""}

weapon-sword-ornate-bloodsteel-1h = Bloodsteel Rapier
    .desc = {""}

weapon-sword-sawblade-bloodsteel-2h = Bloodsteel Sawblade
    .desc = {""}

weapon-sword-sawblade-bloodsteel-1h = Bloodsteel Sawback
    .desc = {""}

weapon-sword-sabre-orichalcum-2h = Orichalcum Sabre
    .desc = {""}

weapon-sword-sabre-orichalcum-1h = Orichalcum Scimitar
    .desc = {""}

weapon-sword-greatsword-orichalcum-2h = Orichalcum Greatsword
    .desc = {""}

weapon-sword-sawblade-orichalcum-2h = Orichalcum Sawblade
    .desc = {""}

weapon-sword-sawblade-orichalcum-1h = Orichalcum Sawback
    .desc = {""}

weapon-sword-longsword-orichalcum-2h = Orichalcum Longsword
    .desc = {""}

weapon-sword-longsword-orichalcum-1h = Orichalcum Shortsword
    .desc = {""}

weapon-sword-katana-orichalcum-2h = Orichalcum Katana
    .desc = {""}

weapon-sword-katana-orichalcum-1h = Orichalcum Swiftblade
    .desc = {""}

weapon-sword-zweihander-orichalcum-2h = Orichalcum Zweihander
    .desc = {""}

weapon-sword-ornate-orichalcum-2h = Orichalcum Ornate Sword
    .desc = {""}

weapon-sword-ornate-orichalcum-1h = Orichalcum Rapier
    .desc = {""}

weapon-axe-poleaxe-bronze-2h = Bronze Poleaxe
    .desc = {""}

weapon-axe-ornate-bronze-2h = Bronze Ornate Axe
    .desc = {""}

weapon-axe-ornate-bronze-1h = Bronze Kilonda
    .desc = {""}

weapon-axe-labrys-bronze-2h = Bronze Labrys
    .desc = {""}

weapon-axe-battleaxe-bronze-2h = Bronze Battleaxe
    .desc = {""}

weapon-axe-battleaxe-bronze-1h = Bronze Cleaver
    .desc = {""}

weapon-axe-axe-bronze-2h = Bronze Axe
    .desc = {""}

weapon-axe-axe-bronze-1h = Bronze Hatchet
    .desc = {""}

weapon-axe-jagged-bronze-2h = Bronze Jagged Axe
    .desc = {""}

weapon-axe-jagged-bronze-1h = Bronze Tomahawk
    .desc = {""}

weapon-axe-greataxe-bronze-2h = Bronze Greataxe
    .desc = {""}

weapon-sceptre-sceptre-hardwood = Hardwood Sceptre
    .desc = {""}

weapon-sceptre-cane-hardwood = Hardwood Cane
    .desc = {""}

weapon-sceptre-arbor-hardwood = Hardwood Arbor
    .desc = {""}

weapon-sceptre-crozier-hardwood = Hardwood Crozier
    .desc = {""}

weapon-sceptre-ornate-hardwood = Hardwood Ornate Sceptre
    .desc = {""}

weapon-sceptre-crook-hardwood = Hardwood Crook
    .desc = {""}

weapon-sceptre-grandsceptre-hardwood = Hardwood Grandsceptre
    .desc = {""}

weapon-sword-longsword-cobalt-2h = Cobalt Longsword
    .desc = {""}

weapon-sword-longsword-cobalt-1h = Cobalt Shortsword
    .desc = {""}

weapon-sword-sawblade-cobalt-2h = Cobalt Sawblade
    .desc = {""}

weapon-sword-sawblade-cobalt-1h = Cobalt Sawback
    .desc = {""}

weapon-sword-greatsword-cobalt-2h = Cobalt Greatsword
    .desc = {""}

weapon-sword-katana-cobalt-2h = Cobalt Katana
    .desc = {""}

weapon-sword-katana-cobalt-1h = Cobalt Swiftblade
    .desc = {""}

weapon-sword-zweihander-cobalt-2h = Cobalt Zweihander
    .desc = {""}

weapon-sword-ornate-cobalt-2h = Cobalt Ornate Sword
    .desc = {""}

weapon-sword-ornate-cobalt-1h = Cobalt Rapier
    .desc = {""}

weapon-sword-sabre-cobalt-2h = Cobalt Sabre
    .desc = {""}

weapon-sword-sabre-cobalt-1h = Cobalt Scimitar
    .desc = {""}
