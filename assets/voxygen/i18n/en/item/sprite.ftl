
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

sprite-crafting_ing-animal_misc-grim_eyeball = Cyclops Eye
    .desc = Looks like it could open an ancient mechanism.

sprite-carrot-carrot = Carrot
    .desc = An orange root vegetable. They say it'll improve your vision!

sprite-cabbage-cabbage = Lettuce
    .desc = A vibrant green leafy vegetable. Lettuce make some salads!

sprite-mushrooms-mushroom-10 = Mushroom
    .desc = Hopefully this one is not poisonous

sprite-food-salad_plain = Plain Salad
    .desc = Literally just chopped lettuce. Does this even count as a salad?

sprite-spore-corruption_spore = Spore of Corruption
    .desc = 
        You feel an evil force pulsating within.
        
        It may be unwise to hold on to it for too long...

sprite-tomato-tomato = Tomato
    .desc = A red fruit. It's not actually a vegetable!

sprite-food-salad_tomato = Tomato Salad
    .desc = Leafy salad with some chopped, juicy tomatoes mixed in.

sprite-food-meat-beast_large_cooked = Cooked Meat Slab
    .desc = Medium Rare.

sprite-food-meat-beast_large_raw = Raw Meat Slab
    .desc = Chunk of beastly animal meat, best after cooking.

sprite-food-meat-beast_small_cooked = Cooked Meat Sliver
    .desc = Medium Rare.

sprite-food-meat-beast_small_raw = Raw Meat Sliver
    .desc = Small hunk of beastly animal meat, best after cooking.

sprite-food-meat-bird_cooked = Cooked Bird Meat
    .desc = Best enjoyed with one in each hand.

sprite-food-meat-bird_large_cooked = Huge Cooked Drumstick
    .desc = Makes for a legendary meal.

sprite-food-meat-bird_large_raw = Huge Raw Drumstick
    .desc = It's magnificent.

sprite-food-meat-bird_raw = Raw Bird Meat
    .desc = A hefty drumstick.

sprite-food-meat-fish_cooked = Cooked Fish
    .desc = A fresh cooked seafood steak.

sprite-food-meat-fish_raw = Raw Fish
    .desc = A steak chopped from a fish, best after cooking.

sprite-food-meat-tough_cooked = Cooked Tough Meat
    .desc = Tastes exotic.

sprite-food-meat-tough_raw = Raw Tough Meat
    .desc = Peculiar bit of meat, best after cooking.

sprite-grass-grass_long_5 = Long Grass
    .desc = Greener than an orc's snout.

sprite-mineral-ore-coal = Coal
    .desc = A combustible black rock that happens to be really good at burning.

sprite-mineral-ore-bloodstone = Bloodstone Ore
    .desc = A deep red ore, it reminds you of blood.

sprite-mineral-ore-coal-coal = Coal
    .desc = A dark, combustible energy source.

sprite-mineral-ore-cobalt = Cobalt Ore
    .desc = A blue, shiny ore.

sprite-mineral-ore-copper = Copper Ore
    .desc = A brown metal. Key part of bronze.

sprite-mineral-ore-gold = Gold Ore
    .desc = A precious yellow metal.

sprite-mineral-ore-iron = Iron Ore
    .desc = An incredibly common but incredibly versatile metal.

sprite-mineral-ore-silver = Silver Ore
    .desc = A precious shiny greyish-white metal.

sprite-mineral-ore-tin = Tin Ore
    .desc = A silvery metal. One of the components of bronze.

sprite-velorite-velorite_ore = Velorite
    .desc = A bizarre, oddly shimmering ore, its origin seems to be shrouded in mystery.

sprite-velorite-velorite = Velorite Fragment
    .desc = Small runes sparkle on its surface, though you don't know what it means.

sprite-mineral-ingot-bloodsteel = Bloodsteel Ingot
    .desc = 
        An alloy of bloodstone and iron, with a dark red color.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-bronze = Bronze Ingot
    .desc = 
        A sturdy alloy made from combining copper and tin.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-cobalt = Cobalt Ingot
    .desc = 
        A strikingly blue ingot.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-copper = Copper Ingot
    .desc = An ingot with a unique brown color.

sprite-mineral-ingot-gold = Gold Ingot
    .desc = An ingot made of refined metallic gold.

sprite-mineral-ingot-iron = Iron Ingot
    .desc = 
        An incredibly commonplace metal.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-orichalcum = Orichalcum Ingot
    .desc = 
        An ingot made of refined orichalcum.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-silver = Silver Ingot
    .desc = An ingot made of refined metallic silver.

sprite-mineral-ingot-steel = Steel Ingot
    .desc = 
        An alloy of iron and coal that is much tougher than its components.
        
        This can be used when crafting metal weapons.

sprite-mineral-ingot-tin = Tin Ingot
    .desc = An ingot primarily used to make bronze.

sprite-mineral-gem-amethystgem = Amethyst
    .desc = A precious purple gem.

sprite-mineral-gem-diamondgem = Diamond
    .desc = A sparkling silver gem.

sprite-mineral-gem-emeraldgem = Emerald
    .desc = A vibrant viridian gem.

sprite-mineral-gem-rubygem = Ruby
    .desc = A superbly scarlet gem.

sprite-mineral-gem-sapphiregem = Sapphire
    .desc = A colorful cobalt gem.

sprite-mineral-gem-topazgem = Topaz
    .desc = An outstanding orange gem.

sprite-wood-item-bamboo = Bamboo
    .desc = 
        A giant woody grass.
        
        This can be used when crafting wooden weapons.

sprite-wood-item-eldwood = Eldwood Logs
    .desc = 
        Old logs that emanate magic.
        
        This can be used when crafting wooden weapons.

sprite-wood-item-frostwood = Frostwood Logs
    .desc = 
        Chilly wood that comes from cold biomes. Cold to the touch.
        
        This can be used when crafting wooden weapons.

sprite-wood-item-hardwood = Hardwood Logs
    .desc = 
        Extra thick and sturdy logs.
        
        This can be used when crafting wooden weapons.

sprite-wood-item-ironwood = Ironwood Logs
    .desc = 
        A particularly sturdy wood.
        
        This can be used when crafting wooden weapons.

sprite-wood-item-wood = Wood Logs
    .desc = 
        Regular, sturdy wooden logs.
        
        This can be used when crafting wooden weapons.

sprite-crafting_ing-abyssal_heart = Abyssal Heart
    .desc = Source of Dagons Power.

sprite-crafting_ing-bowl = Bowl
    .desc = A simple bowl for preparing meals.

sprite-crafting_ing-brinestone = Brinestone
    .desc = Used for armor crafting.

sprite-cacti-flat_cactus_med = Cactus
    .desc = Grows in warm and dry places. Very prickly!

sprite-crafting_ing-coral_branch = Coral Branch
    .desc = Treasure from the bottom of the sea.

sprite-crafting_ing-cotton_boll = Cotton Boll
    .desc = Plucked from a common cotton plant.

sprite-crafting_ing-living_embers = Living Embers
    .desc = The smouldering remains of a fiery creature.

sprite-crafting_ing-oil = Oil
    .desc = A measure of thick, sludgy oil.

sprite-crafting_ing-pearl = Pearl
    .desc = Would make a nice lamp.

sprite-crafting_ing-resin = Resin
    .desc = Used for woodworking.

sprite-seashells-shell-0 = Seashells
    .desc = Shells from a sea creature.

sprite-crafting_ing-sentient_seed = Sentient Seed
    .desc = The undeveloped spawn of a sentient plant.

sprite-crafting_ing-sticky_thread = Sticky Thread
    .desc = A messy spider extract, but a tailor may have use for it.

sprite-rocks-rock-0 = Stones
    .desc = Pebbles from the ground, nothing out of the ordinary.

sprite-twigs-twigs-0 = Twigs
    .desc = Found near trees, a squirrel must've knocked it down.

sprite-crafting_ing-hide-animal_hide = Animal Hide
    .desc = A common pelt from most animals. Becomes leather.

sprite-crafting_ing-hide-carapace = Hard Carapace
    .desc = Tough, hard carapace, a shield to many creatures.

sprite-crafting_ing-hide-dragon_scale = Dragon Scale
    .desc = Tough scale from a legendary beast, hot to the touch.

sprite-crafting_ing-hide-troll_hide = Troll Hide
    .desc = Looted from cave trolls.

sprite-crafting_ing-hide-plate = Plate
    .desc = Durable plate from an armored animal.

sprite-crafting_ing-hide-rugged_hide = Rugged Hide
    .desc = A durable pelt from fierce creatures, favored by leatherworkers.

sprite-crafting_ing-hide-scale = Scale
    .desc = Shiny scales found off of an animal.

sprite-crafting_ing-hide-tough_hide = Tough Hide
    .desc = A relatively tough, rough hide. Becomes leather.

sprite-crafting_ing-animal_misc-claw = Predator Claw
    .desc = 
        Incredibly sharp claw from a predatory animal.
        
        This can be used when crafting weapons.

sprite-crafting_ing-animal_misc-fur = Soft Fur
    .desc = Soft fur from an animal.

sprite-crafting_ing-animal_misc-grim_eyeball-grim_eyeball = Grim Eyeball
    .desc = Casts a petrifying gaze.

sprite-crafting_ing-animal_misc-large_horn = Large Horn
    .desc = 
        A huge sharp horn from an animal.
        
        This can be used when crafting weapons.

sprite-crafting_ing-animal_misc-lively_vine = Lively Vine
    .desc = I think it just moved...

sprite-crafting_ing-animal_misc-phoenix_feather = Phoenix Feather
    .desc = Said to have magical properties.

sprite-crafting_ing-animal_misc-sharp_fang = Sharp Fang
    .desc = 
        Incredibly sharp tooth from a predatory animal.
        
        This can be used when crafting weapons.

sprite-crafting_ing-animal_misc-venom_sac = Venom Sac
    .desc = A venomous sac from a poisonous creature.

sprite-crafting_ing-animal_misc-viscous_ooze = Viscous Ooze
    .desc = A measure of viscous ooze from a slimy creature.

sprite-crafting_ing-leather-leather_strips = Leather Strips
    .desc = Simple and versatile.

sprite-crafting_ing-leather-rigid_leather = Rigid Leather
    .desc = Light but layered, perfect for protection.

sprite-crafting_ing-leather-simple_leather = Simple Leather
    .desc = Light and flexible.

sprite-crafting_ing-leather-thick_leather = Thick Leather
    .desc = Strong and durable.

sprite-crafting_ing-cloth-cloth_strips = Cloth Strips
    .desc = Small and soft, yet useful

sprite-crafting_ing-cloth-cotton = Cotton
    .desc = Easy to work with and multi-functional.

sprite-crafting_ing-cloth-lifecloth = Lifecloth
    .desc = A fabric imbued with the gentleness that nature has to offer.

sprite-crafting_ing-cloth-linen = Linen
    .desc = A textile made from flax fibers.

sprite-crafting_ing-cloth-linen_red = Red Linen
    .desc = A flax fiber textile, dyed to stand out.

sprite-crafting_ing-cloth-moonweave = Moonweave
    .desc = A light yet very sturdy textile.

sprite-crafting_ing-cloth-silk = Silk
    .desc = A fine and strong fibre produced by spiders.

sprite-crafting_ing-cloth-sunsilk = Sunsilk
    .desc = A supernaturally strong textile.

sprite-crafting_ing-cloth-wool = Soft Wool
    .desc = Soft wool from an animal.

sprite-flowers-moonbell = Moonbell
    .desc = It glistens brilliantly under the moonlight.

sprite-crafting_ing-plant_fiber = Plant Fiber
    .desc = A length of raw plant material.

sprite-flowers-pyrebloom = Pyrebloom
    .desc = Warm to the touch, long after picking.

sprite-flowers-flower_red-4 = Red Flower
    .desc = Can be used as a dying ingredient.

sprite-flowers-sunflower_1 = Sunflower
    .desc = Smells like summer.

sprite-flowers-flax = Wild Flax
    .desc = Could be used to spin some simple cloth.

sprite-flowers-sunflower_1-yellow = Yellow Flower
    .desc = Glows like the sun.
