
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

object-key_bone = Bone Key
    .desc = Used to open bone locks. Will break after use.

object-key_haniwa = Haniwa Keystone
    .desc = Used to open doors. Will break after use.

object-key_glass = Glass Key
    .desc = Used to open Glass Barriers. Will break after use.

object-key_rusty-0 = Rusty Tower Key
    .desc = Smells like magic with a bit of... cheese?

object-key_rusty-0-ancient = Ancient Key
    .desc = If you are lucky it works one more time before breaking.

object-key_rusty-0-backdoor = Backdoor Key
    .desc = If you are lucky it works one more time before breaking.

object-key_rusty-0-overseer = Overseer Key
    .desc = If you are lucky it works one more time before breaking.

object-key_rusty-0-smelting = Smelting Room Key
    .desc = If you are lucky it works one more time before breaking.

object-item_cheese = Golden Cheese
    .desc = They say gods eat it to get eternal youth.

object-bomb = Bomb
    .desc = A highly explosive device, demolitionists adore them!

object-v-coin = Coins
    .desc = Precious coins, can be exchanged for goods and services.

object-collar = Collar
    .desc = Tames neutral wild animals within 5 blocks

object-lockpick = Common Lockpick
    .desc = Used to open common locks. Will break after use.

object-training_dummy = Training Dummy
    .desc = His name is William. Fire at will.

object-apple_half = Apple
    .desc = Red and juicy

object-mushroom_curry = Mushroom Curry
    .desc = Who could say no to that?

object-apple_stick = Apple Stick
    .desc = The stick makes it easier to carry!

object-blue_cheese = Blue Cheese
    .desc = Pungent and filling

object-cactus_drink = Cactus Colada
    .desc = Giving you that special prickle.

object-cheese = Dwarven Cheese
    .desc = Made from goat milk from the finest dwarven produce. Aromatic and nutritious!

object-coconut_half = Coconut
    .desc = Reliable source of water and fat. Can often be found growing on palm trees.

object-honeycorn = Honeycorn
    .desc = Sweeet

object-mushroom_stick = Mushroom Stick
    .desc = Roasted mushrooms on a stick for easy carrying

object-pumpkin_spice_brew = Pumpkin Spice Brew
    .desc = Brewed from moldy pumpkins.

object-sunflower_ice_tea = Sunflower Ice Tea
    .desc = Brewed from freshly shelled sunflower seeds

object-potion_red = Potent Potion
    .desc = A potent healing potion.

object-mortar_pestle = Mortar and Pestle
    .desc = Crushes and grinds things into a fine powder or paste. Needed to craft various items.

object-sewing_set = Sewing Set
    .desc = Used to craft various items.

object-potion_empty = Empty Vial
    .desc = A simple glass vial used for holding various fluids.

object-glacial_crystal = Glacial Crystal
    .desc = The purest form of ice, cold enough to cool lava.

object-honey = Honey
    .desc = Stolen from a beehive. Surely the bees won't be happy with this!

object-glowing_remains = Glowing Remains
    .desc = 
        Looted from an evil being.
        
        With some additional work it can surely be
        brought back to its former glory...

object-elegant_crest = Elegant Crest
    .desc = 
        A flawless crest from some majestic creature.
        
        This can be used when crafting weapons.

object-ice_shard = Icy Shard
    .desc = Looted from a frosty creature.

object-long_tusk = Long Tusk
    .desc = 
        A pointy tusk from some beast.
        
        This can be used when crafting weapons.

object-raptor_feather = Raptor Feather
    .desc = A large colorful feather from a raptor.

object-strong_pincer = Strong Pincer
    .desc = 
        The pincer of some creature, it is very tough.
        
        This can be used when crafting weapons.

object-curious_potion = Curious Potion
    .desc = Wonder what this does...

object-potion_agility = Potion of Agility
    .desc = Fly, you fools!

object-potion_red-potion_big = Large Potion
    .desc = Precious medicine, it makes for the largest rejuvenative flask yet.

object-potion_combustion = Potion of Combustion
    .desc = Sets the user ablaze

object-potion_red-potion_med = Medium Potion
    .desc = An innovative invention from an apothecary, better than its smaller precursors.

object-potion_red-potion_minor = Minor Potion
    .desc = A small potion concocted from apples and honey.

object-burning_charm = Blazing Charm
    .desc = Flame is your ally, harness its power to burn your foes.

object-frozen_charm = Freezing Charm
    .desc = Let your enemies feel the sting of cold as you freeze them in their tracks.

object-lifesteal_charm = Siphon Charm
    .desc = Siphon your target life and use it for your own.
