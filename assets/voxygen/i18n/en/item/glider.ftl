
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

glider-basic_red = Red Cloth Glider
    .desc = A simple glider, but with a striking red color.

glider-basic_white = Plain Cloth Glider
    .desc = Simple, but classy.

glider-blue = Blue Falcon
    .desc = Sky colored.

glider-butterfly3 = Moonlit Love
    .desc = Love is in the air.

glider-cloverleaf = Cloverleaf
    .desc = Brings luck to its owner. Light-weight and cheap to make, and also very cute!

glider-leaves = Leaves Glider
    .desc = Soar among the trees

glider-butterfly2 = Orange Monarch
    .desc = The delicate wings flutter faintly.

glider-moonrise = Aquatic Night
    .desc = The stars are beautiful tonight.

glider-butterfly1 = Blue Morpho
    .desc = The delicate wings flutter faintly.

glider-moth = Green Luna
    .desc = The delicate wings flutter faintly.

glider-sandraptor = Sand Raptor Wings
    .desc = Take flight with the wings of a thirsty predator

glider-cultists = Skullgrin
    .desc = Death from above.

glider-snowraptor = Snow Raptor Wings
    .desc = Take flight with the wings of a cold-blooded predator

glider-sunset = Horizon
    .desc = It isn't high noon.

glider-winter_wings = Wings of Winter
    .desc = Sparkles brilliantly and cooly even under the warm sun.

glider-woodraptor = Wood Raptor Wings
    .desc = Take flight with the wings of a stealthy predator
