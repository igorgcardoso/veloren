
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

items-keeper_goggle = Left Goggle-Glass
    .desc = Looks like it could open a door...

items-keeper_goggle-flamekeeper_right = Right Goggle-Glass
    .desc = Looks like it could open a door...
