
### Note to translators.
### Translating this will be *a lot* of work. Concentrate on what's important.
### We don't want you to burn out on 10% of this file and never want to
### contribute.
###
### These files were automatically generated for all possible items in code.
### Some of them are hidden from players, some are only possible to get via
### commands and some simply aren't used anywhere.
###
### Start with other files, translate them first and then maybe come back here.
### If you really want to translate items, start with some subset you know is
### actually used by players.

common-items-testing-test_bag_18_slot = Test 18 slot bag
    .desc = Used for unit tests do not delete

common-items-testing-test_bag_9_slot = Test 9 slot bag
    .desc = Used for unit tests do not delete

common-items-testing-test_boots = Testing Boots
    .desc = Hopefully this test doesn't break!

common-items-npc_armor-pants-leather_blue = Blue Leather Guards
    .desc = {""}

common-items-npc_armor-pants-plate_red = Iron Legguards
    .desc = Greaves forged from iron.

common-items-npc_armor-quadruped_low-dagon = Dagon's Scales
    .desc = Rigid enough to withstand the pressure of the deep ocean.

common-items-npc_armor-quadruped_low-generic = Quad Low Generic
    .desc = Scaly.

common-items-npc_armor-quadruped_low-shell = Quad Low Shell
    .desc = Shell.

common-items-npc_armor-bird_large-phoenix = Phoenix Armor
    .desc = The thickest feather you have ever seen!

common-items-npc_armor-bird_large-wyvern = Wyvern Armor
    .desc = Generic Protection.

common-items-npc_armor-golem-claygolem = Clay Golem Armor
    .desc = Worn by clay golem.

common-items-npc_armor-golem-woodgolem = Wood Golem Armor
    .desc = Yeet

common-items-npc_armor-golem-ancienteffigy = Ancient Effigy Armor
    .desc = Worn by ancient effigy.

common-items-npc_armor-golem-gravewarden = Grave Warden Armor
    .desc = Worn by grave warden.

common-items-npc_armor-biped_small-myrmidon-foot-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-foot-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-foot-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-head-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-head-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-head-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-pants-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-pants-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-pants-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-chest-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-chest-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-chest-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-myrmidon-hand-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-hand-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-hand-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-tail-hoplite = Myrmidon Hoplite
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-tail-marksman = Myrmidon Marksman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-myrmidon-tail-strategian = Myrmidon Strategian
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-foot-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-foot-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-foot-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-head-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-head-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-head-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-pants-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-pants-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-pants-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-chest-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-chest-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-chest-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-sahagin-hand-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-hand-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-hand-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-tail-sniper = Sahagin Sniper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-tail-sorcerer = Sahagin Sorcerer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-sahagin-tail-spearman = Sahagin Spearman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-foot-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-foot-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-foot-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-head-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-head-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-head-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-pants-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-pants-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-pants-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-chest-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-chest-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-chest-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-hand-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-hand-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-hand-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-adlet-tail-hunter = Adlet Hunter
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-tail-icepicker = Adlet Icepicker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-adlet-tail-tracker = Adlet Tracker
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-foot-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-foot-logger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-foot-mugger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-foot-stalker = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-head-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-head-logger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-head-mugger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-head-stalker = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-pants-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-pants-logger = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-pants-mugger = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-pants-stalker = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-chest-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-chest-logger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-chest-mugger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-chest-stalker = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-hand-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-hand-logger = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-hand-mugger = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-hand-stalker = Gnarling
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnarling-tail-chieftain = Gnarling Chieftain
    .desc = Only worn by the most spiritual of Gnarlings.

common-items-npc_armor-biped_small-gnarling-tail-logger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-tail-mugger = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnarling-tail-stalker = Gnarling
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-kappa-foot-kappa = Kappa
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-kappa-head-kappa = Kappa
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-kappa-pants-kappa = Kappa
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-kappa-chest-kappa = Kappa
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-kappa-hand-kappa = Kappa
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-kappa-tail-kappa = Kappa
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-boreal-foot-warrior = Boreal Wrappings
    .desc = The blistering cold makes it hard to move.

common-items-npc_armor-biped_small-boreal-head-warrior = Boreal Helmet
    .desc = Did somebody say...BRAINFREEZE?!

common-items-npc_armor-biped_small-boreal-pants-warrior = Boreal Tunic
    .desc = Colder than the climate it protects you from.

common-items-npc_armor-biped_small-boreal-chest-warrior = Boreal Chestplate
    .desc = So frigid that you can feel it in your heart.

common-items-npc_armor-biped_small-boreal-hand-warrior = Boreal Gauntlets
    .desc = Colder than the touch of death.

common-items-npc_armor-biped_small-bushly-foot-bushly = Bushly
    .desc = Plant Creature

common-items-npc_armor-biped_small-bushly-pants-bushly = Bushly
    .desc = Plant Creature

common-items-npc_armor-biped_small-bushly-chest-bushly = Bushly
    .desc = Plant Creature

common-items-npc_armor-biped_small-bushly-hand-bushly = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-clockwork-foot-clockwork = Clockwork Foot
    .desc = Clockwork Foot.

common-items-npc_armor-biped_small-clockwork-head-clockwork = Clockwork Head
    .desc = Clockwork Head

common-items-npc_armor-biped_small-clockwork-pants-clockwork = Clockwork Pants
    .desc = Clockwork Pants

common-items-npc_armor-biped_small-clockwork-chest-clockwork = Clockwork Chest
    .desc = Clockwork Chest

common-items-npc_armor-biped_small-clockwork-hand-clockwork = Clockwork Hand
    .desc = Clockwork Hand

common-items-npc_armor-biped_small-haniwa-foot-archer = Haniwa Archer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-foot-guard = Haniwa Guard
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-foot-soldier = Haniwa Soldier
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-head-archer = Haniwa Archer
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-head-guard = Haniwa Guard
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-head-soldier = Haniwa Soldier
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-pants-archer = Haniwa Archer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-pants-guard = Haniwa Guard
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-pants-soldier = Haniwa Soldier
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-chest-archer = Haniwa Archer
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-chest-guard = Haniwa Guard
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-chest-soldier = Haniwa Soldier
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-haniwa-hand-archer = Haniwa Archer
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-hand-guard = Haniwa Guard
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-haniwa-hand-soldier = Haniwa Soldier
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-husk-foot-husk = Husk
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-husk-head-husk = Husk
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-husk-pants-husk = Husk
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-husk-chest-husk = Husk
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-husk-hand-husk = Husk
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-husk-tail-husk = Husk
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-flamekeeper-foot-flamekeeper = Flamekeeper Foot
    .desc = Flamekeeper Foot.

common-items-npc_armor-biped_small-flamekeeper-head-flamekeeper = Flamekeeper Head
    .desc = Flamekeeper Head

common-items-npc_armor-biped_small-flamekeeper-pants-flamekeeper = Flamekeeper Pants
    .desc = Flamekeeper Pants

common-items-npc_armor-biped_small-flamekeeper-chest-flamekeeper = Flamekeeper Chest
    .desc = Flamekeeper Chest

common-items-npc_armor-biped_small-flamekeeper-hand-flamekeeper = Flamekeeper Hand
    .desc = Flamekeeper Hand

common-items-npc_armor-biped_small-gnome-foot-gnome = Gnome
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnome-head-gnome = Gnome
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnome-pants-gnome = Gnome
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnome-chest-gnome = Gnome
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnome-hand-gnome = Gnome
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-mandragora-foot-mandragora = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-mandragora-pants-mandragora = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-mandragora-chest-mandragora = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-mandragora-hand-mandragora = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-mandragora-tail-mandragora = Mandragora
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-irrwurz-foot-irrwurz = Irrwurz
    .desc = Plant Creature

common-items-npc_armor-biped_small-irrwurz-pants-irrwurz = Irrwurz
    .desc = Plant Creature

common-items-npc_armor-biped_small-irrwurz-chest-irrwurz = Irrwurz
    .desc = Plant Creature

common-items-npc_armor-biped_small-irrwurz-hand-irrwurz = Irrwurz
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-foot-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-foot-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-foot-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-head-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-head-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-head-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-pants-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-pants-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-pants-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-chest-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-chest-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-chest-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members.

common-items-npc_armor-biped_small-gnoll-hand-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-hand-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-hand-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-tail-rogue = Gnoll Rogue
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-tail-shaman = Gnoll Shaman
    .desc = Ceremonial attire used by members..

common-items-npc_armor-biped_small-gnoll-tail-trapper = Gnoll Trapper
    .desc = Ceremonial attire used by members..

common-items-npc_armor-chest-plate_red = Iron Chestplate
    .desc = A chestplate forged from iron.

common-items-npc_armor-arthropod-generic = Arthropod Armor
    .desc = Worn by arthropods.

common-items-npc_armor-quadruped_medium-frostfang = Frostfang's Thick Skin
    .desc = testing123

common-items-npc_armor-quadruped_medium-roshwalr = Roshwalr's Thick Skin
    .desc = testing123

common-items-npc_armor-quadruped_medium-claysteed = Claysteeds's Thick Skin
    .desc = testing123

common-items-npc_armor-theropod-rugged = Theropod Rugged
    .desc = stronk.

common-items-npc_armor-biped_large-cyclops = Cyclops Armor
    .desc = Made of mysteries.

common-items-npc_armor-biped_large-dullahan = Dullahan Itself Armor
    .desc = Made of It ownself.

common-items-npc_armor-biped_large-generic = Generic Biped Large
    .desc = Worn by bipeds.

common-items-npc_armor-biped_large-gigas_frost = Frost Gigas Armor
    .desc = The best defense is a good offense.

common-items-npc_armor-biped_large-harvester = Harvester Shirt
    .desc = Made of sunflowers.

common-items-npc_armor-biped_large-mindflayer = Mindflayer Armor
    .desc = Worn by mindflayer.

common-items-npc_armor-biped_large-minotaur = Minotaur Armor
    .desc = The best defense is a good offense.

common-items-npc_armor-biped_large-tidal_warrior = Tidal Warrior Armor
    .desc = Made of fish scales.

common-items-npc_armor-biped_large-tursus = Tursus Skin
    .desc = Born with it

common-items-npc_armor-biped_large-warlock = Giant Warlock Chest
    .desc = Made of darkest silk.

common-items-npc_armor-biped_large-warlord = Giant Warlord Chest
    .desc = Made of darkest steel.

common-items-npc_armor-biped_large-yeti = Yeti Hide
    .desc = Strong as Yeti itself.

common-items-npc_armor-biped_large-haniwageneral = Haniwa General Uniform
    .desc = Has seen combat.

common-items-weapons-empty-empty = Empty Item
    .desc = This item may grant abilities, but is invisible

common-items-weapons-sceptre-belzeshrub = Belzeshrub the Broom God
    .desc = 'Is it... alive?'

common-items-armor-alchemist-belt = Alchemist Belt
    .desc = {""}

common-items-armor-alchemist-chest = Alchemist Jacket
    .desc = {""}

common-items-armor-alchemist-hat = Alchemist Hat
    .desc = It seems like a parrot was perched up here.

common-items-armor-alchemist-pants = Alchemist Pants
    .desc = {""}

common-items-armor-misc-head-headband = Headband
    .desc = A simple headband, it's nothing special.

common-items-armor-witch-back = Witch Cape
    .desc = {""}

common-items-armor-witch-belt = Witch Belt
    .desc = {""}

common-items-armor-witch-chest = Witch Robe
    .desc = {""}

common-items-armor-witch-foot = Witch Boots
    .desc = {""}

common-items-armor-witch-hand = Witch Handwarmers
    .desc = {""}

common-items-armor-witch-pants = Witch Skirt
    .desc = {""}

common-items-armor-witch-shoulder = Witch Mantle
    .desc = {""}

common-items-armor-pirate-belt = Pirate Belt
    .desc = {""}

common-items-armor-pirate-chest = Pirate Jacket
    .desc = {""}

common-items-armor-pirate-foot = Pirate Boots
    .desc = {""}

common-items-armor-pirate-hand = Pirate Gloves
    .desc = {""}

common-items-armor-pirate-pants = Pirate Pants
    .desc = {""}

common-items-armor-pirate-shoulder = Pirate Mantle
    .desc = {""}

common-items-armor-miner-back = Miner's Backpack
    .desc = Battered from heavy rocks being carried inside.

common-items-armor-miner-belt = Miner's Belt
    .desc = {""}

common-items-armor-miner-chest = Miner's Vestment
    .desc = Rock dust is covering most of the leather parts.

common-items-armor-miner-foot = Miner's Footwear
    .desc = Someone carved 'Mine!' into the inside.

common-items-armor-miner-hand = Miner's Gloves
    .desc = Someone carved 'Mine!' into the inside.

common-items-armor-miner-pants = Miner's pantaloons.
    .desc = {""}

common-items-armor-miner-shoulder = Miner's Pauldrons.
    .desc = Protects Cave-in and out.

common-items-armor-miner-shoulder_captain = Captain's Pauldrons.
    .desc = {""}

common-items-armor-miner-shoulder_flame = Flamekeeper's Pauldrons.
    .desc = {""}

common-items-armor-miner-shoulder_overseer = Overseer's Pauldrons.
    .desc = {""}

common-items-armor-chef-belt = Chef Belt
    .desc = {""}

common-items-armor-chef-chest = Chef Jacket
    .desc = {""}

common-items-armor-chef-hat = Chef Hat
    .desc = {""}

common-items-armor-chef-pants = Chef Pants
    .desc = {""}

common-items-armor-blacksmith-belt = Blacksmith Belt
    .desc = {""}

common-items-armor-blacksmith-chest = Blacksmith Jacket
    .desc = {""}

common-items-armor-blacksmith-hand = Blacksmith Gloves
    .desc = {""}

common-items-armor-blacksmith-hat = Blacksmith Hat
    .desc = {""}

common-items-armor-blacksmith-pants = Blacksmith Pants
    .desc = {""}

common-items-armor-leather_plate-helmet = Leather Plate Helmet
    .desc = Leather adorned with steel for better protection.

common-items-food-coltsfoot = Coltsfoot
    .desc = A daisy-like flower often used in herbal teas.

common-items-food-dandelion = Dandelion
    .desc = A small, yellow flower. Uses the wind to spread its seeds.

common-items-food-garlic = Garlic
    .desc = Make sure to brush your teeth after eating.

common-items-food-meat = Meat
    .desc = Meat. The lifeblood of mankind.

common-items-food-onion = Onion
    .desc = A vegetable that's made the toughest men cry.

common-items-food-sage = Sage
    .desc = A herb commonly used in tea.

common-items-grasses-medium = Medium Grass
    .desc = Greener than an orc's snout.

common-items-grasses-short = Short Grass
    .desc = Greener than an orc's snout.

common-items-boss_drops-exp_flask = Flask of Velorite Dust
    .desc = Take with plenty of water

common-items-boss_drops-xp_potion = Potion of Skill
    .desc = It doesn't seem to be doing anything...

common-items-mineral-stone-basalt = Basalt
    .desc = A dark volcanic rock, most volcanic rock tends to be basalt..

common-items-mineral-stone-granite = Granite
    .desc = A light-colored igneous rock, coarse-grained and formed by intrusion.

common-items-mineral-stone-obsidian = Obsidian
    .desc = An igneous rock that comes from felsic lava, it never seems to dwindle.

common-items-tool-pickaxe_velorite = Velorite Pickaxe
    .desc = Allows for swift excavation of any ore in sight.

common-items-npc_weapons-biped_small-mandragora = Mandragora
    .desc = Testing

common-items-npc_weapons-biped_small-myrmidon-hoplite = Hoplite Spear
    .desc = {""}

common-items-npc_weapons-biped_small-myrmidon-marksman = Marksman Bow
    .desc = {""}

common-items-npc_weapons-biped_small-myrmidon-strategian = Strategian Axe
    .desc = {""}

common-items-npc_weapons-biped_small-sahagin-sniper = Sniper Bow
    .desc = {""}

common-items-npc_weapons-biped_small-sahagin-sorcerer = Sorcerer Staff
    .desc = {""}

common-items-npc_weapons-biped_small-sahagin-spearman = Spearman Spear
    .desc = {""}

common-items-npc_weapons-biped_small-adlet-hunter = Hunter Spear
    .desc = {""}

common-items-npc_weapons-biped_small-adlet-icepicker = Icepicker Pick
    .desc = {""}

common-items-npc_weapons-biped_small-adlet-tracker = Tracker Bow
    .desc = {""}

common-items-npc_weapons-biped_small-gnarling-chieftain = Chieftain Staff
    .desc = {""}

common-items-npc_weapons-biped_small-gnarling-greentotem = Gnarling Green Totem
    .desc = Yeet

common-items-npc_weapons-biped_small-gnarling-logger = Logger Axe
    .desc = {""}

common-items-npc_weapons-biped_small-gnarling-mugger = Mugger Dagger
    .desc = {""}

common-items-npc_weapons-biped_small-gnarling-redtotem = Gnarling Red Totem
    .desc = Yeet

common-items-npc_weapons-biped_small-gnarling-stalker = Stalker Blowgun
    .desc = {""}

common-items-npc_weapons-biped_small-gnarling-whitetotem = Gnarling White Totem
    .desc = Yeet

common-items-npc_weapons-biped_small-boreal-bow = Boreal Bow
    .desc = {""}

common-items-npc_weapons-biped_small-boreal-hammer = Boreal Hammer
    .desc = {""}

common-items-npc_weapons-biped_small-haniwa-archer = Archer Bow
    .desc = {""}

common-items-npc_weapons-biped_small-haniwa-guard = Guard Spear
    .desc = {""}

common-items-npc_weapons-biped_small-haniwa-soldier = Soldier Sword
    .desc = {""}

common-items-npc_weapons-bow-bipedlarge-velorite = Giant Velorite Bow
    .desc = Infused with Velorite power.

common-items-npc_weapons-bow-saurok_bow = Saurok bow
    .desc = Placeholder

common-items-npc_weapons-axe-gigas_frost_axe = Frost Gigas Axe
    .desc = Placeholder

common-items-npc_weapons-axe-minotaur_axe = Minotaur Axe
    .desc = Placeholder

common-items-npc_weapons-axe-oni_blue_axe = Blue Oni Axe
    .desc = Placeholder

common-items-npc_weapons-staff-bipedlarge-cultist = Giant Cultist Staff
    .desc = The fire gives off no heat.

common-items-npc_weapons-staff-mindflayer_staff = Mindflayer Staff
    .desc = Placeholder

common-items-npc_weapons-staff-ogre_staff = Ogre Staff
    .desc = Placeholder

common-items-npc_weapons-staff-saurok_staff = Saurok Staff
    .desc = Placeholder

common-items-npc_weapons-sword-adlet_elder_sword = Adlet Elder Sword
    .desc = Placeholder

common-items-npc_weapons-sword-bipedlarge-cultist = Giant Cultist Greatsword
    .desc = This belonged to an evil Cult Leader.

common-items-npc_weapons-sword-dullahan_sword = Dullahan Sword
    .desc = Placeholder

common-items-npc_weapons-sword-pickaxe_velorite_sword = Velorite Pickaxe
    .desc = {""}

common-items-npc_weapons-sword-saurok_sword = Saurok Sword
    .desc = Placeholder

common-items-npc_weapons-sword-haniwa_general_sword = Haniwa General Sword
    .desc = Placeholder

common-items-npc_weapons-unique-akhlut = Quad Med Basic
    .desc = testing123

common-items-npc_weapons-unique-asp = Asp
    .desc = testing123

common-items-npc_weapons-unique-basilisk = Basilisk
    .desc = testing123

common-items-npc_weapons-unique-beast_claws = Beast Claws
    .desc = Was attached to a beast.

common-items-npc_weapons-unique-birdlargebasic = Bird Large Basic
    .desc = testing123

common-items-npc_weapons-unique-birdlargebreathe = Bird Large Breathe
    .desc = testing123

common-items-npc_weapons-unique-birdlargefire = Bird Large Fire
    .desc = Fiery touch of a mighty aerial beast

common-items-npc_weapons-unique-birdmediumbasic = Bird Medium Basic
    .desc = BiteBiteBite!!! FightFightFight!!!

common-items-npc_weapons-unique-bushly = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-cardinal = Caduceus
    .desc = The snakes seem to be alive

common-items-npc_weapons-unique-clay_golem_fist = Clay Golem Fists
    .desc = Yeet.

common-items-npc_weapons-unique-clockwork = Clockwork
    .desc = testing123

common-items-npc_weapons-unique-cloudwyvern = Cloud Wyvern
    .desc = testing123

common-items-npc_weapons-unique-coral_golem_fist = Coral Golem Fists
    .desc = Yeet

common-items-npc_weapons-unique-crab_pincer = Crab Pincer
    .desc = testing123

common-items-npc_weapons-unique-dagon = Dagon Kit
    .desc = Ocean Power!

common-items-npc_weapons-unique-deadwood = Deadwood
    .desc = testing123

common-items-npc_weapons-unique-driggle = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-emberfly = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-fiery_tornado = FieryTornado
    .desc = Fiery Tornado weapon

common-items-npc_weapons-unique-flamekeeper_staff = Flamekeeper Staff
    .desc = Flamekeeper Staff

common-items-npc_weapons-unique-flamethrower = Flamethrower
    .desc = Throwing Flames

common-items-npc_weapons-unique-flamewyvern = Flame Wyvern
    .desc = testing123

common-items-npc_weapons-unique-frostfang = Frostfang
    .desc = testing123

common-items-npc_weapons-unique-frostwyvern = Frost Wyvern
    .desc = testing123

common-items-npc_weapons-unique-haniwa_sentry = Haniwa Sentry
    .desc = Rotating turret weapon

common-items-npc_weapons-unique-hermit_alligator = Hermit Alligator Teeth
    .desc = Grrr!

common-items-npc_weapons-unique-husk = Husk
    .desc = testing123

common-items-npc_weapons-unique-husk_brute = Husk Brute
    .desc = testing123

common-items-npc_weapons-unique-icedrake = Ice Drake
    .desc = testing123

common-items-npc_weapons-unique-irrwurz = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-maneater = Maneater
    .desc = testing123

common-items-npc_weapons-unique-mossysnail = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-organ = Organ Aura
    .desc = Motivational Tune

common-items-npc_weapons-unique-quadlowbasic = Quad Low Basic
    .desc = testing123

common-items-npc_weapons-unique-quadlowbeam = Quad Small Beam
    .desc = testing123

common-items-npc_weapons-unique-quadlowbreathe = Quad Low Breathe
    .desc = testing123

common-items-npc_weapons-unique-quadlowquick = Quad Low Quick
    .desc = testing123

common-items-npc_weapons-unique-quadlowtail = Quad Low Tail
    .desc = testing123

common-items-npc_weapons-unique-quadmedbasic = Quad Med Basic
    .desc = testing123

common-items-npc_weapons-unique-quadmedbasicgentle = Quad Med Basic
    .desc = testing123

common-items-npc_weapons-unique-quadmedcharge = Quad Med Charge
    .desc = testing123

common-items-npc_weapons-unique-quadmedhoof = Quad Med Hoof
    .desc = testing123

common-items-npc_weapons-unique-quadmedjump = Quad Med Jump
    .desc = testing123

common-items-npc_weapons-unique-quadmedquick = Quad Med Quick
    .desc = testing123

common-items-npc_weapons-unique-quadsmallbasic = Quad Small Basic
    .desc = testing123

common-items-npc_weapons-unique-roshwalr = Roshwalr
    .desc = testing123

common-items-npc_weapons-unique-sea_bishop_sceptre = Sea Bishop Sceptre
    .desc = Hits like a wave.

common-items-npc_weapons-unique-seawyvern = Sea Wyvern
    .desc = testing123

common-items-npc_weapons-unique-simpleflyingbasic = Simple Flying Melee
    .desc = I believe I can fly!!!!!

common-items-npc_weapons-unique-stone_golems_fist = Stone Golem's Fist
    .desc = Was attached to a mighty stone golem.

common-items-npc_weapons-unique-theropodbasic = Theropod Basic
    .desc = testing123

common-items-npc_weapons-unique-theropodbird = Theropod Bird
    .desc = testing123

common-items-npc_weapons-unique-theropodcharge = Theropod Charge
    .desc = testing123

common-items-npc_weapons-unique-theropodsmall = Theropod Small
    .desc = testing123

common-items-npc_weapons-unique-tidal_claws = Tidal Claws
    .desc = Snip snap

common-items-npc_weapons-unique-tidal_totem = Tidal Totem
    .desc = Yeet

common-items-npc_weapons-unique-tornado = Tornado
    .desc = Tornado weapon

common-items-npc_weapons-unique-treantsapling = Starter Grace
    .desc = Fret not, newbies shant cry.

common-items-npc_weapons-unique-turret = Turret
    .desc = Turret weapon

common-items-npc_weapons-unique-tursus_claws = Tursus Claws
    .desc = Was attached to a beast.

common-items-npc_weapons-unique-wealdwyvern = Weald Wyvern
    .desc = testing123

common-items-npc_weapons-unique-wendigo_magic = Wendigo Magic
    .desc = spook.

common-items-npc_weapons-unique-wood_golem_fist = Wood Golem Fists
    .desc = Yeet

common-items-npc_weapons-unique-ancient_effigy_eyes = Ancient Effigy Eyes
    .desc = testing123

common-items-npc_weapons-unique-claysteed = Claysteed Hoof
    .desc = testing123

common-items-npc_weapons-unique-gravewarden_fist = Gravewarden Fist
    .desc = testing123

common-items-npc_weapons-unique-arthropods-antlion = Antlion
    .desc = testing123

common-items-npc_weapons-unique-arthropods-blackwidow = Black Widow
    .desc = testing123

common-items-npc_weapons-unique-arthropods-cavespider = Cave Spider
    .desc = testing123

common-items-npc_weapons-unique-arthropods-dagonite = Dagonite
    .desc = testing123

common-items-npc_weapons-unique-arthropods-hornbeetle = Horn Beetle
    .desc = testing123

common-items-npc_weapons-unique-arthropods-leafbeetle = Leaf Beetle
    .desc = testing123

common-items-npc_weapons-unique-arthropods-mosscrawler = Moss Crawler
    .desc = testing123

common-items-npc_weapons-unique-arthropods-tarantula = Tarantula
    .desc = testing123

common-items-npc_weapons-unique-arthropods-weevil = Weevil
    .desc = testing123

common-items-npc_weapons-hammer-bipedlarge-cultist = Giant Cultist Warhammer
    .desc = This belonged to an evil Cult Leader.

common-items-npc_weapons-hammer-cyclops_hammer = Cyclops Hammer
    .desc = Placeholder

common-items-npc_weapons-hammer-harvester_scythe = Harvester Sythe
    .desc = Placeholder

common-items-npc_weapons-hammer-ogre_hammer = Ogre Hammer
    .desc = Placeholder

common-items-npc_weapons-hammer-oni_red_hammer = Red Oni Hammer
    .desc = Placeholder

common-items-npc_weapons-hammer-troll_hammer = Troll Hammer
    .desc = Placeholder

common-items-npc_weapons-hammer-wendigo_hammer = Wendigo Hammer
    .desc = Placeholder

common-items-npc_weapons-hammer-yeti_hammer = Yeti Hammer
    .desc = Placeholder

common-items-modular-weapon-primary-bow-bow = Bow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-composite = Composite Bow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-greatbow = Greatbow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-longbow = Longbow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-ornate = Ornate Bow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-shortbow = Shortbow Limbs
    .desc = {""}

common-items-modular-weapon-primary-bow-warbow = Warbow Limbs
    .desc = {""}

common-items-modular-weapon-primary-axe-axe = Axe Head
    .desc = {""}

common-items-modular-weapon-primary-axe-battleaxe = Battleaxe Head
    .desc = {""}

common-items-modular-weapon-primary-axe-greataxe = Greataxe Head
    .desc = {""}

common-items-modular-weapon-primary-axe-jagged = Jagged Axe Head
    .desc = {""}

common-items-modular-weapon-primary-axe-labrys = Labrys Head
    .desc = {""}

common-items-modular-weapon-primary-axe-ornate = Ornate Axe Head
    .desc = {""}

common-items-modular-weapon-primary-axe-poleaxe = Poleaxe Head
    .desc = {""}

common-items-modular-weapon-primary-staff-brand = Brand Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-grandstaff = Grandstaff Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-longpole = Long Pole Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-ornate = Ornate Staff Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-pole = Pole Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-rod = Rod Shaft
    .desc = {""}

common-items-modular-weapon-primary-staff-staff = Staff Shaft
    .desc = {""}

common-items-modular-weapon-primary-sword-greatsword = Greatsword Blade
    .desc = {""}

common-items-modular-weapon-primary-sword-katana = Katana Blade
    .desc = {""}

common-items-modular-weapon-primary-sword-longsword = Longsword Blade
    .desc = {""}

common-items-modular-weapon-primary-sword-ornate = Ornate Sword Blade
    .desc = {""}

common-items-modular-weapon-primary-sword-sabre = Sabre Blade
    .desc = {""}

common-items-modular-weapon-primary-sword-sawblade = Sawblade
    .desc = {""}

common-items-modular-weapon-primary-sword-zweihander = Zweihander Blade
    .desc = {""}

common-items-modular-weapon-primary-sceptre-arbor = Arbor Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-cane = Cane Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-crook = Crook Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-crozier = Crozier Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-grandsceptre = Grandsceptre Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-ornate = Ornate Sceptre Shaft
    .desc = {""}

common-items-modular-weapon-primary-sceptre-sceptre = Sceptre Shaft
    .desc = {""}

common-items-modular-weapon-primary-hammer-greathammer = Greathammer Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-greatmace = Greatmace Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-hammer = Hammer Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-maul = Maul Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-ornate = Ornate Hammer Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-spikedmace = Spiked Mace Head
    .desc = {""}

common-items-modular-weapon-primary-hammer-warhammer = Warhammer Head
    .desc = {""}

common-items-crafting_ing-rock = Rock
    .desc = Made up of a bunch of different minerals, looks like it would hurt to get hit with.

common-items-crafting_ing-animal_misc-bone = Thick Bone
    .desc = A thick bone, it seems sturdy enough to craft with.

common-items-crafting_ing-animal_misc-ember = Ember
    .desc = A flicking ember left by a fiery creature.

common-items-crafting_ing-animal_misc-feather = Feather
    .desc = Feather from a bird.

common-items-tag_examples-cultist = Anything related to cultists
    .desc = These items are a little creepy.

common-items-tag_examples-gnarling = Attire of the Gnarling tribes
    .desc = Worn by Gnarlings and their Chieftains.

common-items-flowers-blue = Blue Flower
    .desc = Matches the color of the sky.

common-items-flowers-pink = Pink Flower
    .desc = Looks like a lollipop.

common-items-flowers-white = White flower
    .desc = Pure and precious.
